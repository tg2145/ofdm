#//these are the results on Tom's personal laptop

asyncTimes = [ 10794 11140 11332 11840 12363 12867 13397 14669 15888 ];
serialTimes = [ 11120 11367 11606 12116 12689 13279 13884 15455 17007 ];
means = [ 1000 1024.5 1049.65 1099.5 1149.74 1199.69 1249.93 1376.62 1498.74 ];
variances = [ 0 208.047 830.473 3334.01 7539.91 13364.3 20849.6 46847 83332.9 ];
stdDevs = [ 0 14.4238 28.8179 57.7409 86.8326 115.604 144.394 216.442 288.674 ];

#//use these to see if there is a pattern between any values and the performance difference
#//weaker core enabled results, we can see the weaker core is driving the serial speed

asyncTimesARM = [ 12301 12638 12900 13488 14086 14664 15242 16735 18125 ];
serialTimesARM = [ 20711 21279 21754 22832 23746 24713 25724 28280 30802 ];