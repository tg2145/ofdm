%// 4 worker threads
%// async time
as4wT = [10948 11123 11191 11825 12250 12950 13598 14823 16140 17328 18576 19890 21114 23609 26160 31159 36095 48781 61126 ];
%// serial time
se4wT = [11096 11440 11586 12202 12707 13294 13701 15454 17173 18737 20422 22122 23685 26944 30208 36917 43702 60545 77189 ];
%// mean
mea4w = [1000 1024.5 1049.65 1099.5 1149.74 1199.69 1249.93 1376.62 1498.74 1624.13 1749.62 1877.5 1998.89 2250.63 2501.49 3003.04 3493.32 4756.44 5988.69 ];
%// deviation
dev4w = [0 14.4238 28.8179 57.7409 86.8326 115.604 144.394 216.442 288.674 361.665 432.916 507.439 577.772 719.723 867.398 1152.24 1441.68 2168.55 2893.91 ];
%// coeff of variance
cov4w = dev4w./mea4w;
%// performance difference between serial and async
diff4w = 100*(((1./as4wT) - (1./se4wT))./(1./se4wT));

%// 12 worker threads  
%// async time
as12wT = [10691 10887 11077 11592 12082 12614 13155 14578 15775 17124 18303 19515 20751 23309 25947 31000 36006 48474 61002 ];
%// serial time
se12wT = [10764 10893 11184 11830 12504 13235 13979 15856 17781 19776 21811 23707 25714 29625 33823 41656 49517 69022 88370 ];
%// mean
mea12w = [1000 1024.47 1049.46 1099.35 1149.04 1199.51 1249.25 1375.74 1499.42 1624.06 1748.87 1876.31 1999.68 2250.77 2499.98 2999.4 3498.33 4747.83 5992.83 ];
%// deviation
dev12w = [0 14.4275 28.8683 57.8639 86.7179 115.544 144.305 216.867 288.909 361.493 433.163 505.432 577.68 721.316 866.156 1154.16 1442.72 2170.54 2890.07 ];
%// coeff of variance
cov12w = dev12w./mea12w;
%// performance difference between serial and async
diff12w = 100*(((1./as12wT) - (1./se12wT))./(1./se12wT));

%// 4 worker threads, shorter sleep times
%// async time
as4wTs = [1553 1573 1600 1647 1696 1746 1798 1922 2048 2167 2302 2437 2564 2824 3078 3584 4089 5363 6630 ];
%// serial time
se4wTs = [1556 1575 1602 1657 1713 1772 1832 1982 2141 2312 2474 2654 2807 3170 3504 4147 4840 6511 8276 ];
%// mean
mea4ws = [100 102.001 104.494 109.435 114.473 119.402 124.502 136.987 149.654 162.182 174.207 187.097 199.496 225.016 249.736 299.691 349.929 476.622 598.741 ];
%// deviation
dev4ws = [0 1.41501 2.86712 5.76077 8.63952 11.5064 14.4238 21.748 28.8179 36.2076 43.2641 50.7487 57.7409 72.4455 86.8326 115.604 144.394 216.442 288.674 ];
%// coeff of variance
cov4ws = dev4ws./mea4ws;
%// performance difference between serial and async
diff4ws = 100*(((1./as4wTs) - (1./se4wTs))./(1./se4wTs));

%// 12 worker threads, shorter sleep times
%// async time
as12wTs = [1544 1564 1588 1636 1681 1735 1779 1904 2035 2158 2286 2416 2540 2799 3047 3555 4055 5328 6570 ];
%// serial time
se12wTs = [1559 1578 1606 1665 1730 1801 1869 2052 2241 2434 2641 2844 3044 3437 3819 4598 5373 7355 9347 ];
%// mean
mea12ws = [100 101.998 104.496 109.496 114.486 119.474 124.466 137.07 149.458 162.005 174.315 186.916 199.349 224.565 249.042 299.507 349.251 475.74 599.417 ];
%// deviation
dev12ws = [0 1.41561 2.87175 5.77229 8.6503 11.533 14.4275 21.6725 28.8683 36.1284 43.3111 50.5208 57.8639 72.2945 86.7179 115.544 144.305 216.867 288.909 ];
%// coeff of variance
cov12ws = dev12ws./mea12ws;
%// performance difference between serial and async
diff12ws = 100*(((1./as12wTs) - (1./se12wTs))./(1./se12wTs));

%// 4 worker threads, shorter sleep times, 1 weak worker
%// async time
as4wTsW = [1720 1743 1775 1823 1890 1944 2005 2144 2295 2438 2584 2736 2873 3181 3470 4045 4633 6103 7600 ];
%// serial time
se4wTsW = [2557 2607 2663 2770 2886 2995 3099 3364 3630 3872 4120 4387 4630 5122 5619 6656 7610 10217 13154 ];
%// mean
mea4wsW = [100 102.001 104.494 109.435 114.473 119.402 124.502 136.987 149.654 162.182 174.207 187.097 199.496 225.016 249.736 299.691 349.929 476.622 598.741 ];
%// deviation
dev4wsW = [0 1.41501 2.86712 5.76077 8.63952 11.5064 14.4238 21.748 28.8179 36.2076 43.2641 50.7487 57.7409 72.4455 86.8326 115.604 144.394 216.442 288.674 ];
%// coeff of variance
cov4wsW = dev4wsW./mea4wsW;
%// performance difference between serial and async
diff4wsW = 100*(((1./as4wTsW) - (1./se4wTsW))./(1./se4wTsW));

%// 12 worker threads, shorter sleep times, 1 weak worker
%// async time
as12wTsW = [1591 1615 1643 1694 1745 1796 1844 1982 2107 2240 2372 2511 2644 2904 3167 3696 4219 5548 6851 ];
%// serial time
se12wTsW = [2535 2442 2632 2718 2847 2932 3014 3292 3582 3826 4119 4302 4557 5080 5544 6591 7641 10270 12940 ];
%// mean
mea12wsW = [100 101.998 104.496 109.496 114.486 119.474 124.466 137.07 149.458 162.005 174.315 186.916 199.349 224.565 249.042 299.507 349.251 475.74 599.417 ];
%// deviation
dev12wsW = [0 1.41561 2.87175 5.77229 8.6503 11.533 14.4275 21.6725 28.8683 36.1284 43.3111 50.5208 57.8639 72.2945 86.7179 115.544 144.305 216.867 288.909 ];
%// coeff of variance
cov12wsW = dev12wsW./mea12wsW;
%// performance difference between serial and async
diff12wsW = 100*(((1./as12wTsW) - (1./se12wTsW))./(1./se12wTsW));

%// 4 worker threads, shorter sleep times, 1 strong worker
%// async time
as4wTsS = [1378 1396 1417 1456 1497 1537 1578 1679 1783 1887 1985 2098 2200 2400 2609 3017 3430 4454 5468 ];
%// serial time
se4wTsS = [1566 1585 1607 1659 1713 1768 1825 1962 2118 2279 2431 2593 2746 3088 3413 4047 4676 6272 7946 ];
%// mean
mea4wsS = [100 102.001 104.494 109.435 114.473 119.402 124.502 136.987 149.654 162.182 174.207 187.097 199.496 225.016 249.736 299.691 349.929 476.622 598.741 ];
%// deviation
dev4wsS = [0 1.41501 2.86712 5.76077 8.63952 11.5064 14.4238 21.748 28.8179 36.2076 43.2641 50.7487 57.7409 72.4455 86.8326 115.604 144.394 216.442 288.674 ];
%// coeff of variance
cov4wsS = dev4wsS./mea4wsS;
%// performance difference between serial and async
diff4wsS = 100*(((1./as4wTsS) - (1./se4wTsS))./(1./se4wTsS));

%// 12 worker threads, shorter sleep times, 1 strong worker
%// async time
as12wTsS = [1479 1496 1523 1571 1614 1662 1700 1822 1939 2055 2173 2296 2414 2647 2876 3349 3813 4986 6131 ];
%// serial time
se12wTsS = [1557 1584 1606 1666 1731 1795 1866 2046 2226 2420 2624 2829 3030 3415 3793 4570 5345 7296 9222 ];
%// mean
mea12wsS = [100 101.998 104.496 109.496 114.486 119.474 124.466 137.07 149.458 162.005 174.315 186.916 199.349 224.565 249.042 299.507 349.251 475.74 599.417 ];
%// deviation
dev12wsS = [0 1.41561 2.87175 5.77229 8.6503 11.533 14.4275 21.6725 28.8683 36.1284 43.3111 50.5208 57.8639 72.2945 86.7179 115.544 144.305 216.867 288.909 ];
%// coeff of variance
cov12wsS = dev12wsS./mea12wsS;
%// performance difference between serial and async
diff12wsS = 100*(((1./as12wTsS) - (1./se12wTsS))./(1./se12wTsS));

figure 1;
plot(cov4w, diff4w, "color", "k", "linewidth", 2,
     cov4ws, diff4ws, "linestyle", "--", "color", "k", "linewidth", 2,
     cov12w, diff12w, "color", "g", "linewidth", 2,
     cov12ws, diff12ws, "linestyle", "--", "color", "g", "linewidth", 2);
grid on;
legend("4 workers",
       "4 workers (shorter processing)",
       "12 workers",
       "12 workers (shorter processing)");
title("Async vs Serial Buffer Management Performance with respect to Coefficient of Variance of workload");
xlabel("Coefficient of Variance");
ylabel("Percentage Improvement of Async Management Performance");

figure 2;
plot(cov4wsW, diff4wsW, "color", "k", "linewidth", 2,
     cov4wsS, diff4wsS, "linestyle", "--", "color", "k", "linewidth", 2,
     cov12wsW, diff12wsW, "color", "g", "linewidth", 2,
     cov12wsS, diff12wsS, "linestyle", "--",  "color", "g", "linewidth", 2);
grid on;
legend("4 workers, 1 weak worker",
       "4 workers, 1 strong worker",
       "12 workers, 1 weak worker",
       "12 workers, 1 strong worker");
title("Async vs Serial Buffer Management Performance with respect to Coefficient of Variance of workload");
xlabel("Coefficient of Variance");
ylabel("Percentage Improvement of Async Management Performance");
