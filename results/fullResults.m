%laptop_131072 IFFT len_results in samples per second
l_131072_r = [7.6e7 1.33e8 1.71e8 2.03e8 1.94e8 1.94e8 1.79e8 1.79e8];
%laptop_131072 IFFT len_worker counts
l_131072_w = [1 2 3 4 5 6 7 8];

l_98304_r = [7.26e7 1.28e8 1.70e8 2.04e8 1.99e8 2.04e8 1.88e8 2.15e8];
l_98304_w = [1 2 3 4 5 6 7 8];

l_65536_r = [8.65e7 1.53e8 2.03e8 2.44e8 2.39e8 2.39e8 2.41e8 2.74e8];
l_65536_w = [1 2 3 4 5 6 7 8];

l_32768_r = [9.52e7 1.73e8 2.29e8 2.75e8 2.48e8 2.63e8 2.93e8 3.15e8];
l_32768_w = [1 2 3 4 5 6 7 8];

l_16384_r = [1.03e8 1.93e8 2.64e8 3.28e8 3.29e8 3.12e8 3.54e8 3.77e8];
l_16384_w = [1 2 3 4 5 6 7 8];

l_8192_r = [1.16e8 2.06e8 2.95e8 3.79e8 3.85e8 3.49e8 3.66e8 4.07e8];
l_8192_w = [1 2 3 4 5 6 7 8];

l_4096_r = [2.09e8 3.04e8 3.71e8 3.40e8 3.73e8 3.87e8 4.00e8];
l_4096_w = [2 3 4 5 6 7 8];

l_2048_r = [3.97e8 3.50e8 3.88e8 4.18e8 4.21e8];
l_2048_w = [4 5 6 7 8];


figure 1;
plot(l_131072_w, l_131072_r, "color", "k", "linewidth", 2,
     l_98304_w, l_98304_r, "linestyle", "--", "color", "k", "linewidth", 2,
     l_65536_w, l_65536_r, "color", "g", "linewidth", 2,
     l_32768_w, l_32768_r, "linestyle", "--", "color", "g", "linewidth", 2,
     l_16384_w, l_16384_r, "color", "r", "linewidth", 2,
     l_8192_w, l_8192_r, "linestyle", "--", "color", "r", "linewidth", 2,
     l_4096_w, l_4096_r, "color", "b", "linewidth", 2,
     l_2048_w, l_2048_r, "linestyle", "--", "color", "b", "linewidth", 2);
grid on;
legend("Length: 2**17",
    "Length: 3*(2**15)",
    "Length: 2**16",
    "Length: 2**15",
    "Length: 2**14",
    "Length: 2**13",
    "Length: 2**12",
    "Length: 2**11");
title("Laptop CP-OFDM PHY Transceiver Performance");
xlabel("Worker Count for each PHY");
ylabel("Processing Rate in Samples per Second");


%pc_131072 IFFT len_results in samples per second
p_131072_r = [2.18e8 3.83e8 4.66e8 3.02e8 2.02e8 1.87e8 1.72e8 1.54e8 1.39e8 1.30e8];
%pc_131072 IFFT len_worker counts
p_131072_w = [1 2 3 4 5 6 7 8 9 10];

p_98304_r = [2.21e8 4.31e8 6.05e8 5.14e8 3.43e8 2.82e8 2.34e8 1.97e8 1.59e8 1.44e8];
p_98304_w = [1 2 3 4 5 6 7 8 9 10];

p_65536_r = [2.31e8 4.44e8 6.13e8 7.00e8 6.57e8 5.76e8 4.49e8 3.51e8 2.57e8 2.15e8];
p_65536_w = [1 2 3 4 5 6 7 8 9 10];

p_32768_r = [2.68e8 4.72e8 6.69e8 8.34e8 1.01e9 9.64e8 9.61e8 1.08e9 1.07e9 8.86e8];
p_32768_w = [1 2 3 4 5 6 7 8 9 10];

p_16384_r = [2.99e8 8.98e8 1.16e9 1.28e9 1.37e9 1.45e9 1.34e9 1.42e9 1.47e9];
p_16384_w = [1 3 4 5 6 7 8 9 10];

p_8192_r = [3.38e8 6.46e8 9.54e8 1.44e9 1.48e9 1.62e9 1.69e9 1.70e9];
p_8192_w = [1 2 3 6 7 8 9 10];

p_4096_r = [3.96e8 5.70e8 7.32e8 8.22e8 9.86e8 1.15e9];
p_4096_w = [2 3 4 5 6 7];

p_2048_r = [4.15e8 5.00e8 6.00e8 6.83e8 7.66e8 8.49e8 9.05e8];
p_2048_w = [4 5 6 7 8 9 10];

figure 2;
plot(p_131072_w, p_131072_r, "color", "k", "linewidth", 2,
     p_98304_w, p_98304_r, "linestyle", "--", "color", "k", "linewidth", 2,
     p_65536_w, p_65536_r, "color", "g", "linewidth", 2,
     p_32768_w, p_32768_r, "linestyle", "--", "color", "g", "linewidth", 2,
     p_16384_w, p_16384_r, "color", "r", "linewidth", 2,
     p_8192_w, p_8192_r, "linestyle", "--", "color", "r", "linewidth", 2,
     p_4096_w, p_4096_r, "color", "b", "linewidth", 2,
     p_2048_w, p_2048_r, "linestyle", "--", "color", "b", "linewidth", 2);
legend("Length: 2**17",
    "Length: 3*(2**15)",
    "Length: 2**16",
    "Length: 2**15",
    "Length: 2**14",
    "Length: 2**13",
    "Length: 2**12",
    "Length: 2**11");
grid on;
title("PC CP-OFDM PHY Transceiver Performance");
xlabel("Worker Count for each PHY");
ylabel("Processing Rate in Samples per Second");
