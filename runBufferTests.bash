baseSpeed=1000
baseVariations=(1 50 100 200 300 400 500 750 1000 1250 1500 1750 2000 2500 3000 4000 5000 7500 10000)
for var in ${baseVariations[@]}; do
    ./build/src/bufferComp 4 $baseSpeed $var
done
