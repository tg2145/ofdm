\contentsline {chapter}{\numberline {1}Introduction}{5}%
\contentsline {chapter}{\numberline {2}Background}{9}%
\contentsline {section}{\numberline {2.1}Industry Standard}{9}%
\contentsline {section}{\numberline {2.2}Hardware vs Software}{11}%
\contentsline {section}{\numberline {2.3}SDR Development}{12}%
\contentsline {section}{\numberline {2.4}Waveform Basics}{13}%
\contentsline {chapter}{\numberline {3}Research}{16}%
\contentsline {section}{\numberline {3.1}Literature Review}{16}%
\contentsline {section}{\numberline {3.2}OFDM}{20}%
\contentsline {chapter}{\numberline {4}Software Development}{27}%
\contentsline {section}{\numberline {4.1}Contributions}{27}%
\contentsline {section}{\numberline {4.2}Asynchronous Buffer Manager}{28}%
\contentsline {subsection}{\numberline {4.2.1}Introduction}{28}%
\contentsline {subsection}{\numberline {4.2.2}Implementation}{31}%
\contentsline {section}{\numberline {4.3}CP-OFDM Workers}{35}%
\contentsline {subsection}{\numberline {4.3.1}Setup}{35}%
\contentsline {subsection}{\numberline {4.3.2}TX Worker}{36}%
\contentsline {subsection}{\numberline {4.3.3}RX Worker}{38}%
\contentsline {subsection}{\numberline {4.3.4}Other Considerations}{40}%
\contentsline {section}{\numberline {4.4}CP-OFDM Frame Detection}{40}%
\contentsline {section}{\numberline {4.5}OFDM Parameters}{43}%
\contentsline {section}{\numberline {4.6}Other Implementations}{46}%
\contentsline {chapter}{\numberline {5}Results}{48}%
\contentsline {section}{\numberline {5.1}Signal Analysis}{48}%
\contentsline {section}{\numberline {5.2}CP-OFDM PHY Performance}{49}%
\contentsline {subsection}{\numberline {5.2.1}Introduction}{49}%
\contentsline {subsection}{\numberline {5.2.2}Laptop Performance}{50}%
\contentsline {subsection}{\numberline {5.2.3}Custom PC Performance}{52}%
\contentsline {section}{\numberline {5.3}CP-OFDM Frame Detection}{54}%
\contentsline {section}{\numberline {5.4}Asynchronous Buffer Manager}{56}%
\contentsline {subsection}{\numberline {5.4.1}Introduction}{56}%
\contentsline {subsection}{\numberline {5.4.2}Expectations}{57}%
\contentsline {subsection}{\numberline {5.4.3}Performance}{58}%
\contentsline {chapter}{\numberline {6}Conclusions}{63}%
\contentsline {section}{\numberline {6.1}Accomplishments}{63}%
\contentsline {section}{\numberline {6.2}Shortcomings}{64}%
\contentsline {section}{\numberline {6.3}Future Work}{66}%
\contentsline {chapter}{Bibliography}{68}%
\contentsline {chapter}{\numberline {A}Code Snippets}{72}%
\contentsline {section}{\numberline {A.1}Main}{72}%
\contentsline {section}{\numberline {A.2}PHY Parameters}{73}%
\contentsline {section}{\numberline {A.3}Asynchronous Buffer Manager}{75}%
\contentsline {subsection}{\numberline {A.3.1}Pick-up Manager}{75}%
\contentsline {subsection}{\numberline {A.3.2}Drop-off Manager}{79}%
\contentsline {subsection}{\numberline {A.3.3}Pick-up Manager Serial Refactor}{84}%
\contentsline {subsection}{\numberline {A.3.4}Drop-off Manager Serial Refactor}{88}%
\contentsline {subsection}{\numberline {A.3.5}Performance Comparison Test}{93}%
\contentsline {subsection}{\numberline {A.3.6}Unit Test for Order Preservation}{98}%
\contentsline {section}{\numberline {A.4}CP-OFDM Workers}{103}%
\contentsline {subsection}{\numberline {A.4.1}TX PHY Worker}{103}%
\contentsline {subsection}{\numberline {A.4.2}RX PHY Worker}{106}%
\contentsline {section}{\numberline {A.5}CP-OFDM Frame Detection}{109}%
