\chapter{Background}
\section{Industry Standard}
Waveform development is commonly accomplished on hardware, one method being Field Programmable Gate Array (FPGA) development.
Another method is to mix FPGA development with software, implementing the most computationally expensive tasks on the FPGA.
In fact, the Ettus X300 SDR comes with an FPGA, specifically designed to allow for this hybrid development if desired.
The most extreme case for hardware development is application-specific integrated circuits (ASIC), which is the least flexible and most
time consuming to implement. The hardware route (whether it be ASIC or reliance on FPGA) has been the default path for waveform development 
for a while as the performance trade off was simply not a reasonable decision to make even just a few years ago. A graph showing these 
trade-offs is given in \cite{ieee:7} and is shown in figure \ref{fig:devComp} with GPP development having highest ability for rapid prototyping and reconfigurability.
I would go further and say SDRs have more potential than simply prototyping and can be used for commercial waveform development, granted this paper is from 2016 so 
GPPs were significantly weaker back then, which may be why the authors specifically chose to say SDRs are mainly for creating prototypes, as that was the most appropriate use at the time.
\begin{figure}
    \centering
    \includegraphics[width=12cm,keepaspectratio]{pics/compGraph.png}
    \caption{"Trade-off between reconfigurability and development time for FPGA,
    ASIC, DSP, GPP, and hybrid GPP/FPGA-centric SDR architectures" taken from \cite{ieee:7}}
    \label{fig:devComp}
\end{figure}
Over the years GPP performance has exponentially grown to the point where these default assumptions of needing hardware should 
be challenged, a significant amount of computational performance can be leveraged if software and modern processors are used to their advantages.
A software based waveform implementation will commonly use a lower level programming language such as C++,
although there are no limitations other than performance needs and what a specific language can achieve. The
work presented in this thesis will be completed entirely in C++ as the API used to interface with the RF hardware used
is also implemented in and made for C++ development. C++ offers the high performance needed to develop waveforms
with significant performance needs.\\\\
ANDRO Computational Solutions has performed case studies and has found that software-only waveform development is 3-5x faster than FPGA development \cite{andro}.
This increase in development speed paired with the significantly lower cost of entry due to expensive tools and licenses needed to develop on FPGA and other 
hardware makes software based development very attractive if it is known that the performance goals can be reached.
\section{Hardware vs Software}
These different development methods come with the obvious trade offs. For this section, hardware development will focus
on FPGA development, but the points will still hold for all hardware based development in general. An FPGA based waveform will
almost certainly have higher performance than a software based application if done correctly, there is no disputing that.
The most crucial question to be asked when it comes to deciding if FPGA should or needs to be used is if that extra performance is needed.
It is a very difficult question to answer if one does not have any data points to refer to in terms of this performance
difference, which is why exploring this development and setting a benchmark is significant. These benchmarks are obviously irrelevant if the techniques
used to get high performance in software are not also documented, which is why they should also be tied to this work. What can be accomplished in software, and how, are needed to make the 
best decisions. Due to the exponential growth of GPP performance, the answer to this question may shock FPGA and other hardware developers that have put the blinders on, 
along with potential customers that may be unaware of what can be accomplished with a software only approach on a modern GPP.\\\\
Software development is significantly quicker than hardware development, along with 
the valuable addition of flexibility and the ability to rapidly throw out prototypes throughout the development process.
This allows for rapid prototyping in software only approaches, which is valuable information for the development team and customers
as project time moves forward. For an FPGA approach, it can take months before there is anything to show for the work, whereas software
prototypes can be thrown out in the matter of weeks. Since software implementations are quicker and more flexible, it is also 
easier to implement a waveform specification iteratively and demonstrate progress and abilities with more resolution. 
If it is determined that the waveform performance needs can be met through software, then all the advantages go to the
software development route. The only reason one might still choose to go with the FPGA or hardware route is inexperience with software based development,
or a strict requirements to develop on FPGA or some other hardware.
\section{SDR Development}
Software based waveform development requires RF hardware that the software can interface with, this is where
Software Defined Radios (SDRs) come into play. There are plenty of solutions available at many cost levels,
ranging from under \$50 to tens of thousands of dollars. While the upper range may seem very high, it still pales in comparison
to the hardware based alternatives. A high performance SDR can be used for testing and development of any software based waveform, so there is plenty
of reuse once the investment is made. The SDR solution will naturally be driven by cost and performance needs.
For this work, an Ettus X300 equipped with a UBX-160 daughterboard will be used, an approximate cost of \$8300 \cite{ettus:uhd,ettus:ubx}. 
This SDR is considered a higher end radio, with a maximum sample rate of 200MHz and a maximum instantaneous bandwidth of 160MHz.
For the sake of some comparison and context, a low cost solution from the same brand, Ettus, is the B205mini-i \cite{ettus:b205}.
The B205mini-i has an approximate cost of \$1300 and has a max sample rate and instantaneous bandwidth of 61.44MHz and 56MHz respectively.
Ettus provides an open source driver for their radios, UHD \cite{ettus:uhd,ettus:uhddoc}, which is used to interface with all of their SDRs.
This is another great advantage to these radios and software development in general. The UHD API uses the same commands 
between all Ettus radios (apart for some very specific 
commands that only higher end SDRs support), meaning that once a waveform is built to support an Ettus SDR, you can for the most part, plug and play with RF hardware.
The main considerations to make in this case is to verify the requested sample rate, bandwidth, etc, are all supported
by the SDR hardware you wish to use. For the sake of this research, since the X300 is the highest performance SDR that I have at my disposal for this work and the focus is high performance,
the ability to swap any Ettus radio will not come into play, but it is an important ability automatically built-in when the software is created. 
When improved hardware is released (ex. Ettus releases a new radio that has higher performance than the X300, and a new processor is released), it will allow for the work presented here
to be reused and possibly showcase even higher performance without requiring any changes to the software. This ability to swap/support numerous radios at multiple price points 
demonstrates the flexibility software allows.\\\\
The next question a skeptic may ask is about the reliance on Ettus products and the UHD driver. Since this is software, 
there is nothing stopping developers from adding support for multiple other radios into their software.
Once the software for those new radios and drivers is developed, it can be reused in future waveform development and built up as more SDR applications become 
available, speeding up future work.
\section{Waveform Basics}
A waveform will consist of a MAC (Media Access Control) and a PHY (Physical Layer). This work will strictly focus on the 
PHY layer as that is where the application of OFDM occurs. This work assumes a developer already has a MAC or plans to create one,
and is making a decision on the path they will take for PHY development (a possible consideration being a hybrid FPGA application where the PHY is implemented on an FPGA). 
Another reason for the focus on a PHY rather than a MAC is the PHY typically carries the 
most computational load, so this is a driving decision on whether or not a software implementation will be explored. The primary focus of this thesis is to research and determine what
level of performance is possible in a software based OFDM PHY on a run of the mill GPP, in this case an Intel laptop. A higher performance PC was also tested on to provide
more information on what performance can be expected on higher end systems, as that may be closer to the target platform in many circumstances.
The MAC layer is above the PHY and performs the management. It will manage what data, synchronization sequences, and everything else the waveform needs to transmit or process on the receiver end,
and supplies the PHY layer with the data or commands to perform those tasks.
The PHY would then pick up this data, processes it into an OFDM signal, and then transmit it. The reverse order would happen on the 
receiver end, where an OFDM signal would be detected, processed, and data would be sent to the MAC layer to be decoded and processed.
The MAC can also control what processes the PHY performs, in this case the MAC would be able to tell the PHY to enter the initial 
search for the OFDM frame detection, and once the signal is found, to switch to tracking mode where the correlator can skip to the approximate
location of the next frame to operate more efficiently. This tracking stage is also used to determine if we have lost the signal to allow the MAC to switch back to initial search.
The point here is the MAC is in control of all the decision making for the waveform, 
and the PHY's job is to perform the computationally expensive signal processing.\\\\
In regards to the performance of this PHY, simply throwing together some code will obviously not be sufficient for most applications, certainly not enough to
come to a conclusion on what performance is possible.
A perfect example is if one were to use the Fourier Transform, it would be highly inefficient and result in poor performance. A waveform developer
would no doubt use the FFT algorithm to perform the Fourier Transform, but there is nothing stopping an newbie from fruitlessly attempting to
implement the Fourier Transform due to inexperience and lack of knowledge. This can be applied for many other methods used in signal processing, 
or waveform processing in general; the ability to accomplish tasks effectively in software is crucial for creating high performance applications. 
There are a plethora of references and software prototypes for almost any digital signal processing technique you can think of, many of which
may use inefficient methods, which is fine for what they are typically designed for. While these sources are a great teaching tool, there
is much more needed to accomplish what this work sets out to do.
Real time software applications are not as common to find, as the prototypes and resources are typically put together for educational purposes, 
or proof of concept, so there is no goal to run this software in real time, which would not be possible anyways in their form. It is possible to find some applications,
but they all tend to use low performance methods that only allow for very basic waveforms to be run in real time, like an FM receiver \cite{gnu:fm}. A low performance radio development 
toolkit exists that engineers tend to use for their SDR development and testing, which will be discussed and is precisely what I mean when referring to developers using inefficient methods. 