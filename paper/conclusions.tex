\chapter{Conclusions}
\section{Accomplishments}
This work has shown that a software only OFDM PHY implementation can boast very high performance on a GPP if done correctly, well exceeding 1B samples per second under certain 
parameters on a high performance PC. To the best of my knowledge this is the highest performance software only OFDM PHY transceiver.  Given the architecture of today's GPPs, a worker thread architecture is absolutely necessary along with several other techniques and optimizations
to allow for such performance. It also shows how quick software development can be, as the majority of the time was spent researching and documenting my work, the actual software development
took only around 4-5 weeks starting from scratch. Given my experience and the ability to reuse most of this code for any other OFDM based waveform, an OFDM PHY similar to this can easily be accomplished in
3 weeks or less, giving more time to develop the rest of the waveform components. This time span is significantly shorter than the months it would take to make an equivalent implementation in FPGA.
The development also used all open source and free tools, meaning my cost to develop this software was \$0.\\\\
Apart from my overall goal being reached of maxing out the hardware capabilities of an Ettus X300, my contributions are also general enough to be applied to many more software applications.
The ABM can be used to optimize existing worker thread architectures, especially on ARM big.LITTLE architecture. If a developer needs to use different data types and buffers, they can use 
whatever data structures they need as the ABM has been implemented as a template. If more than one buffer is needed, along with other metadata, a data structure can be created and passed in as the template.\\\\
The OFDM frame detection can also be implemented into any other waveform that requires auto correlation with constant delay. The input does not need to be an OFDM signal for this frame detection to work.
As it stands, it does require the input to be the native UHD driver sample type, complex 16-bit integers. This can be converted to a template as future work if a developer requires a different 
data type. It should be noted that fixed point should be used for this correlator as rounding errors will add up if floating point is used. How much of
an impact these rounding errors will have has not been determined.\\\\
The OFDM workers also have high reuse, as they can easily be adjusted if a waveform has slightly different specs from what was used in this work. Changing the QPSK modulation for the 
subcarriers has been used as an example several times now, the function calls to modulate and demodulate could be swapped out with a new modulation type. This goes for all other aspects of the 
workers as well, for example the cyclic prefix insertion can be removed and DFT-s-OFDM can be attempted instead.
\section{Shortcomings}
While my goals have been reached and the work overall has been successful, there are many shortcomings for this work. A complete PHY would contain more components than what was accomplished
here. None of these directly relate to OFDM so they weren't a concern when it comes to presenting the performance that was achievable. For example, resampling and filtering were mentioned, neither
of these were implemented into the PHY, even though a complete PHY would certainly contain these two components. Frequency and phase correction were also not implemented in the PHY, two crucial 
components for recovering a received signal. Another component is error correction. Unfortunately the time frame for this work did not allow for any of these components to be researched and implemented.\\\\
While these components will affect the overall performance of the waveform, as new GPPs are released the performance this software will be able to achieve will end up crossing out the extra computation
needed by those components. Since those PHY components are not specific to OFDM, they have also already been implemented in existing waveforms by ANDRO, meaning that it is known that these components
can efficiently be implemented in software and executed in real time, as they already have been. The current performance is so much higher than the Ettus X300 hardware supports that there is plenty of 
processing power left for these components, so that is not a concern. Of course these should eventually be implemented and the performance should be officially documented, as it will be lower than the 
results presented here for a complete waveform; the exact amount is important to know.\\\\
Another shortcoming is some issues that still exist in the software. This was seen in some of the presented results, some combinations of parameters caused a segmentation fault, which would need to be 
resolved in a production release. There is also a double free crash that sometimes occurs when the waveform shuts down. This does not affect the waveform while it is running, but would also need to be 
resolved in an official release. My guess is there is a bug with some of the FFTW3 calls I made that are not thread safe, as it is a FFTW3 free call that causes this crash. Since neither
of these issues affect the results once the software has started and is running, they were not mentioned in the results and are not a huge concern in relation to what this thesis wanted to accomplish and present.\\\\
Another shortcoming is the normalization of the CP-OFDM frame detection. The results are currently not being normalized properly, meaning the results are not between 0 and 1. This means I had to increase the threshold
and analyze the values coming from the detection to create a reasonable threshold for detection. Regardless, the peak of the correlation is in the correct location, meaning it does identify the framing 
of the signal, which is what truly matters. Normalization is not specifically necessary for frame detection anyways, and many times not even used, but it is my goal as it makes it easier to define a threshold for the frame detection.
A better implementation might be able to adjust this threshold dynamically, making this normalization unnecessary. This is another one of those issues that were not addressed due to the priority of the issue along with the given time frame.
\section{Future Work}
There is more future work that can be done for this project, this work has barely scratched the surface as it only gives a glimpse into what kind of performance is possible on a GPP.
For one, all other components that would be included in a complete PHY would be implemented and used to remeasure the performance that can be achieved. The most difficult and computationally
expensive component would be error correction, as identified in \cite{ieee:3}. Some error correction algorithms are complicated enough that their software implementation and optimization could be a thesis in itself. Of course
this depends on what error correction algorithm is used, one could spend forever trying to implement and test every possible algorithm. The bugs that still exist in the software that were mentioned in the 
previous section should also be troubleshot and fixed.\\\\
Relating to completing the PHYs, the receiver also would need to have a thread created to receive samples from the UHD driver to support an SDR. Only the transmitter has this implemented currently due to an easier implementation 
as samples are simply being pushed to the driver. To receive samples, additional buffering would need to be implemented to allow for the collection of samples and prevent the UHD driver from overrunning. Tying right into adding radio 
support to the receiver is adding more radio support to the PHYs in general. Right now this software is only built to support UHD devices. Additional software can be written to allow for interfacing between
more sets of SDR hardware, making the software even more flexible. This is necessary if we wish to run signals at sample rates higher than possible on the X300. The Motorola NS-1 has its own 
driver that the software would need to interface with to test these higher rates.\\\\
The frame detection also needs to have its stages implemented. The testing and performance measurements were done with different sample spans rather than measuring the actual stages as the handling for that
was not created. As far as measuring performance, the implementation of these states is not necessary, but to allow a MAC to control this frame detection when a signal is running through the receiver in real time, 
this control would need to exist. This implementation would make more sense once the receiver could process an incoming signal for a radio, it would not make sense to spend time on this work until that
is completed first when it comes to the overall development.\\\\
There is also still more optimization that could be done to squeeze out more performance, mainly AVX SIMD optimization. This was not attempted as the -O3 flag does a decent enough job at optimizing the code already,
but manually writing the SIMD code yourself, as you know the exact processing and patterns for what you are trying to accomplish, will typically yield higher performance than the compiler.\\\\
Since I have only looked at a software implementation of a PHY, the obvious next step would be to implement a MAC in software to control this PHY. This would allow us to run this software as a complete
waveform and move data across it. A specification would need to be defined for this work as well.\\\\
Another test would be the replace the sleep times in the buffer management comparison test to use real processing and perform the test on different ARM systems to get real life results rather than
attempting to make estimations through simulations. I attempted to build the test software on my phone to run this test but was not successful. This would be interesting to look further into, 
along with running the OFDM PHY on an ARM system to document what an ARM device is capable of.