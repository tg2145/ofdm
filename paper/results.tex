\chapter{Results}
\section{Signal Analysis}
The reliable and easy way to verify signal transmission is to use a spectrum analyzer. The main verification here is to make sure the spectrum looks as expected based
on the waveform parameters supplied to the waveform. It is actually a good thing I did this as the first time I measured the spectrum it did not look correct, leading me to 
finding a bug in how I was setting the bins for the IFFT operation. This is not something that could have been found without this step as my FFT subcarrier recovery was 
built based off my incorrect implementation of the IFFT bin setting. Another issue found was that I was not scaling up the sample values high enough, i.e. not using enough of the
DAC range for the radio, leading to a spike at the center frequency. This spike was presumed to be leakage from the amplifier from within the radio. Regardless of the true cause,
using more of the DAC range by scaling the samples up fixed this issue as the power of the signal overtook this peak. Figure \ref{fig:specAn} shows the spectrum for one of the tests. 
The bandwidth for this setup was 160MHz with a 5MHz frequency guard on both sides, which should result in a 150MHz bandwidth measured on the spectrum analyzer.
\begin{figure}[h]
    \centering
    \includegraphics[width=12cm,keepaspectratio]{pics/specAn.png}
    \caption{Spectrum Analyzer Capture}
    \label{fig:specAn}
\end{figure}
The capture shown clearly measures 149MHz, some roll off is not being included within the measurement which will account for the last 1MHz to get us to a bandwidth of 150MHz.
This confirms that the CP-OFDM TX PHY can in fact correctly process and transmit an OFDM signal in real time on SDR hardware. The setup used to take this measurement is given in figure \ref{fig:specSetup}.
\begin{figure}[h]
    \centering
    \includegraphics[width=12cm,keepaspectratio]{pics/specSetup.png}
    \caption{Spectrum Analyzer Capture Setup}
    \label{fig:specSetup}
\end{figure}
\section{CP-OFDM PHY Performance}
\subsection{Introduction}
A software only simulation was used to test the PHY in transceiver mode under multiple different conditions. Two systems were used to quantify what kind of performance can be expected
with a regular (but higher end) laptop and on a high end custom PC. Multiple IFFT/FFT lengths were tested as a way to demonstrate the performance we can expect under different
subcarrier counts. The exact amount of subcarriers that the IFFT/FFT length will map to depends on the frequency guards used. Since this frequency guard will not have a significant effect
on performance I will not worry about it. The waveform designer can also choose to use however many of those bins within the FFT length as they please, which would also adjust the subcarrier count, and expect the same
performance that was measured here, so subcarrier count will also not be considered in the testing. Since this is a worker based architecture the last parameter will be the worker count used by each PHY. This means a setup with 4 workers will have 
4 TX PHY workers and 4 RX PHY workers, a total of 8 workers in the end to end system. The average sample rate the software only simulation is able to process over some period of time 
is what will be used as a way to quantify performance. This tells us the maximum sample rate supported without doing any resampling. This seems to be the fairest and 
most consistent way to measure performance as needlessly upsampling a signal doesn't properly represent what the GPP can truly process, as upsampling does not increase the throughput of the signal.
\subsection{Laptop Performance}
The first system that was tested on was an i7 laptop. The specs for this system are given in figure \ref{fig:laptopSpec}.
\begin{figure}[h]
    \centering
    \includegraphics[width=12cm,keepaspectratio]{pics/laptopSpec.png}
    \caption{Laptop Specs}
    \label{fig:laptopSpec}
\end{figure}
Since three parameters were used for the testing, the X axis will represent the worker count for each PHY, the Y axis will display the performance in samples per second, and the legend to sort each line
by IFFT/FFT length. Note that "**" represents "to the power of", ex. 2**17 is a length of 131,072. Lengths with power of 2 were chosen as they are the most efficient sizes that the IFFT and FFT algorithm
can work on as it is a divide and conquer algorithm. This is another one of those subtle ways performance was optimized. There is also a length that multiplies a power of 2 by 3,
FFTW3 is able to also efficiently work on lengths that have a power of 2 factor along with factors of 3 and 5. That length was chosen to add more resolution as the maximum tested length was reached.
Figure \ref{fig:laptopResults} shows the results.
\begin{figure}[h]
    \centering
    \includegraphics[width=14cm,keepaspectratio]{pics/laptopResults.png}
    \caption{CP-OFDM PHY Laptop Results}
    \label{fig:laptopResults}
\end{figure}
It is clear that even for the very large FFT length of 131,072 the laptop was still able to reach the maximum sample rate of an Ettus X300, with the smaller lengths exceeding more than double the maximum sample rate.
This shows that even a GPP on a laptop can process OFDM signals with relative ease, with performance leveling at 4 workers per PHY. This makes sense based on the architecture of the system, where there are
8 physical cores hyper-threaded to 16 threads. While the 16 threads allow the system to be in the middle of up to 16 tasks all in parallel, it can only physically execute 8 threads in parallel. The best analogy here
is a cashier and a checkout lane. The cashier would be the physical core, and the checkout lane is the thread. When hyper-threading each physical core to two threads, the cashier is receiving two lanes to 
process. While the cashier will now be checking out two customers at once, they can only physically work on one lane at a time, moving back and forth based on scheduling and demands.
\subsection{Custom PC Performance}
A custom PC was also tested to see what can be achieved on a high end system, the specs for this PC are given in figure \ref{fig:pcSpec}.
\begin{figure}[h]
    \centering
    \includegraphics[width=12cm,keepaspectratio]{pics/pcSpec.png}
    \caption{Custom PC Specs}
    \label{fig:pcSpec}
\end{figure}
The same parameters as the laptop test were used along with the method of measurement. The main difference here from the laptop results is more workers were tested as the system had more cores available.
\begin{figure}[h]
    \centering
    \includegraphics[width=14cm,keepaspectratio]{pics/pcResults.png}
    \caption{CP-OFDM PHY Custom PC Results}
    \label{fig:pcResults}
\end{figure}
Interestingly the same pattern was not present on the PC, the performance did not cap once all the physical cores were used. This may be due to the PC being significantly higher performance
than the laptop, but the exact reason is not of concern for the scope of this work. This PC performed significantly better than the laptop, reaching processing speeds over 1.5B samples per second.
The lowest rate was obviously the longest FFT length, which is expected, but still well exceeded the capabilities of an Ettus X300 with a processing rate of 466M samples per second. This shows that a 
high performance GPP is very capable of running very high sample rates in a software only implementation.\\\\
For reference, the largest IFFT/FFT size used in 5GNR is 4096 (or 2**12), showing that this PHY is not only very capable at this maximum size, but it can be pushed significantly further and still
easily process high sample rates (relative to a high end SDR). This shows that custom OFDM waveforms with high subcarrier counts are feasible in a software only implementation.\\\\
This graph does have some clear gaps, the most obvious being the 2**12 results ending at 7 workers per PHY. When running through all these combinations, some issues within the software were exposed. For some reason
these parameter combinations were resulting in segmentation faults. There may be some bug in the code initialization causing these parameter combinations to fail, every other combination that passed the initialization stage was stable.
This will be further addressed in the conclusions.
\section{CP-OFDM Frame Detection}
The nature of the frame detection algorithm should be explained further before any results are presented. Since the first correlation requires the complete computation and subsequent correlations reuse 
calculations from previous results, as the span that the processing speed is averaged over increases, the performance will also seem to increase. In reality after the first expensive correlation, or overhead,
the processing speed will remain extremely fast and consistent. Since this overhead is required every time the correlation begins it will be included in the measurements.\\\\
Since the initial processing will tend to be over significantly longer lengths of time to locate the framing of the signal, it will appear to run faster during this stage. It needs to be made clear that while yes,
the algorithm itself will average a faster processing speed, it will not actually run faster in relation to the overall signal as the tracking will skip significant chunks of the signal and omit a large portion 
of the processing. This will mean the overhead will play a larger role in the tracking stage speed, but will overall still result in higher performance when the tracking stage processing is looked at in general.\\\\
Thanks to the nature of the algorithm, apart from the overhead time, the processing speed of this algorithm will be consistent regardless of the cyclic prefix (i.e. the length of the correlation). Regardless
of the cyclic prefix length, one sample will always exit the correlation, and another sample will take its place, meaning the computation, and thus the processing speed, will be consistent. Testing was
performed over several cyclic prefix lengths to verify this theory and it held true. The following results will apply to all cyclic prefix lengths and OFDM symbol lengths, since that will drive the 
cyclic prefix length.\\\\
The following results were measured on the laptop described in figure \ref{fig:laptopSpec}. I first started by measuring the performance over a time span of 50 samples, which may be the time span used
during the tracking stage. This time span was picked arbitrarily and gives us a decent estimate for how fast we can expect each tracking iteration to take. The overall processing speed in relation 
to the signal itself will be significantly faster as it will skip larger portions of the signal to get to the estimated location for the next OFDM symbol alignment; it will not be considered for these
results as it will vary greatly based on symbol length. When the value is presented it will also be clear why the symbol length does not need to be considered, as the speed is already high enough
to process a signal in real time.\\\\
The performance was then measured over a span of 60,000 samples. This will be used to give a better estimate as to what performance can be expected during the initial detection stage. Depending on where 
the frame detection begins in relation to the framing of the signal this time span can vary significantly. Again, due to the nature of this algorithm, the average processing speed will fall between these two values 
if the time span calculated over also falls between these two time spans. The speed is already sufficient so measuring larger or shorter time spans is unnecessary. The results are as follows:
\begin{description}
    \item[$\bullet$ Time span of 50 samples] This span was chosen to simulate processing speed during the tracking stage. Regardless of symbol length and cyclic prefix length the processing speed was
                                             consistently around 900M-1000M samples per second, or an overall time of around 50ns to process this time span. Given that the max sample rate of the 
                                             Ettus X300 is 200M samples per second and the tracking stage will already be skipping most of the incoming samples, this processing speed is more than 
                                             high enough to process the signal in real time.
    \item[$\bullet$ Time span of 60,000 samples] This span was chosen to simulate processing speed during the initial frame detection stage. Regardless of symbol length and cyclic prefix length the processing speed was
                                              consistently around 750B samples per second, or an overall time of around 80ns to process this time span. This is also more than sufficient to process the incoming signal
                                              in real time for an Ettus X300 at the highest possible sample rate.
\end{description}
Based on these results, this optimization has resulted in enough performance to run the frame detection in real time on very high sample rates. While the method for measuring the speed and the calculations do 
seem correct (as I have not been able to prove them wrong), I am in disbelief when it comes to the processing rate over 60,000 samples. Even if I ignore this result, the slower processing speed for a span of 50 samples
is already more than enough, and we know that larger spans will have even faster processing rates. If we were to assume the initial frame detection stage also runs at around 1000M samples per second, based on the 
concept of duty cycle to measure CPU utilization, only 20 percent CPU utilization on one thread will be needed for initial frame sync on an X300 running at the max sample rate of 200MHz.
\section{Asynchronous Buffer Manager}
\subsection{Introduction}
In terms of the results for the ABM, the correct ordering of the buffers from end to end under many worker counts proves that the implementation has been successful from a logical and managerial standpoint.
It now comes time to lay out the expectations for this manager when it comes to performance in relation to the problems it aims to solve, idle time introduced by serial buffer management. To make this 
comparison as fair as possible, a copy of the ABM was created and the management was refactored to change the buffer passing from asynchronous to serial. This keeps all internal computations and management
identical, the main difference is the manager now keeps track of who is the next worker in line to receive a buffer and waits for that worker to make a request, just as the worker would do in the ABM.\\\\
This refactor requires one additional parameter upon startup, the worker count. The manager needs to know the worker count so it knows when to roll over
the ID for the next processing iteration, and the worker must pass the ID in each interaction with the manager so the manager knows how to handle the interaction. The worker must identify itself when it interacts with 
the manager because this manager is request based, so it needs to know who is making the request to correctly pass the buffers in a serial order. If a worker makes a request out of order, which is still allowed,
the manager will use a mutex to block the request until it is that worker's time in line. The blocking of requests so they are executed in order of ID is what changes the management from asynchronous to serial buffer passing, 
as the requests are now executed in serial order of worker ID rather than FIFO.
\subsection{Expectations}
The expectations are that the ABM will perform better than the serial manager as idle time increases in the serial manager. This idle time is created by variation in the worker processing times, if one worker finishes 
sooner than the worker preceding it, the faster worker will sit in an idle state until the preceding worker finishes and receives its next buffer first. In the asynchronous implementation this faster worker would send
its request for a buffer before the preceding worker and have its request fulfilled since the requests are handled FIFO and not in a serial order, therefore eliminating this idle time.\\\\
The processing speed difference is clear if equations are created for the overall processing speed for the worker distribution based on the buffer passing method. I will first start with the asynchronous
buffer manager. For the simplicity of these equations I will assume the interaction time between the workers is negligible and strictly focus on the worker processing itself. Since each worker will run independently
and have its requests fulfilled immediately, each worker will have its own average processing speed and all the average processing speeds will be summed up to get the overall processing speed of the worker distribution.
The formula for this equation is as follows:
\begin{align*}
    Overall Speed &= \sum_{n=0}^{W} p_n
\end{align*}
Where:\\
W = the number of workers in the distribution\\
p = the average processing speed of the worker\\\\
Now, for the serial manager, this equation requires some more thinking behind it. Since all manager interactions are assumed to be negligible, this means all workers upon startup are fulfilled instantaneously.
The next iteration of processing can only begin once all workers have finished their previous iteration of processing, meaning the slowest worker will drive this time. There will be some slight shift in where this
holdup occurs based on which worker is the slowest, but the data passing will always get stuck and wait at this slowest worker, meaning each complete iteration turnover will take as long as the processing time for this worker.
This leads us to the equation for this overall processing speed of the serial manager:
\begin{align*}
    Overall Speed &= \sum_{n=0}^{W} p_m
\end{align*}
Where:\\
W = the number of workers in the distribution\\
p = the average processing speed of the worker\\
m = the index of the slowest worker\\\\
As clearly shown in this equation, the slowest worker will eat up all the extra processing speed of the other workers, which is the idle time of the system. While these equations do not tell us exactly what 
the performance difference will be, it makes it clear which method is superior. The best case scenario for the serial manager is that the slowest worker in the distribution is equal to all other workers in the distribution.
In this case, the equations would be equal, and asynchronous management would provide no performance increase; but it should be noted that it will also not provide any decrease in performance either.
\subsection{Performance}
Since the equations given above are theoretical estimations, real measurements should be performed to see how my expectations align with reality, as the interactions with the manager will of course play some (but I doubt much)
factor into the processing speed difference along with other factors in the system that cannot be controlled thanks to the operating system. I will also test under different simulation circumstances to give some quantifications 
as to what the performance difference is in that specific case. As I noted, I expect the performance to be identical when all the workers process at the same speed, that will be a benchmark for my measurements.
This testing is also where the big.LITTLE ARM architecture will finally come back into play, as the ABM was designed with this exact architecture in mind.\\\\
The testing simulations will use sleep commands in the workers to simulate processing speed. These sleep times will be randomly generated with parameters adjusting the range of these sleep times.
By using randomly generated sleep times, I can reset the seed to regenerate the same values and make measurements for the simulation. The main parameter for these times
that will be presented is the coefficient of variation. The coefficient of variation was chosen as it best coincides with the idle time experienced in the serial manager. 
Since it is a normalization of the variation, it allows us to display what effect, if any, different average processing times will have. Figure \ref{fig:abmPerf} shows the performance difference
between the ABM and the serial refactor. The X axis is the coefficient of variation and the Y axis is the performance difference between the ABM and the serial refactor. A value of 0 means no improvement,
and a value of 100 would mean the ABM is 100 percent, or twice as fast as its serial counterpart. The legend is sorted by worker count and processing speed, 4 and 12 workers were tested here. Two 
different processing speeds were chosen to see what effect the average processing speed will play in terms of the performance difference.
\begin{figure}[h]
    \centering
    \includegraphics[width=14cm,keepaspectratio]{pics/abmPerf.png}
    \caption{ABM vs Serial Manager Performance Difference}
    \label{fig:abmPerf}
\end{figure}
As expected, the performance was nearly identical between the two methods when there was no variation between the worker processing, i.e. all workers had the same processing speed. As more idle time is added,
an increase of the coefficient of variation, the performance difference increases as predicted. Interestingly, distributions with more workers result in a larger performance difference, this was not something 
I thought would happen; but I do see it as a good surprise. Simply changing how requests are processed can result in over a 40 percent performance increase on workloads with high variation.\\\\
It was also observed that the average processing speed did not affect the performance improvement by much, but shorter processing speeds do offer less of an improvement. This meets my expectations as I figured the 
interactions with the manager would factor more into the overall processing speed and eat up some of the time saving. Regardless, the performance improvement is similar and can be significant. The most important note here
is there is no performance decrease at any point, which makes this management method favorable in every situation, as it can only improve the worker distribution performance.\\\\
The next test was to simulate the ARM big.LITTLE architecture. This was done by adjusting one of the workers to be a "big" or "LITTLE" worker. For simplicity this means the processing speed, or sleep time, would 
be scaled based on which designation each worker was given. A "big" worker in this test was signified as a "strong" worker and the sleep time would be halved to simulate this worker being twice as fast as the other workers.
A "LITTLE" worker would be signified as a "weak" worker in this test and the sleep time would be doubled to simulate this worker being twice as slow as the rest of the workers. Figure \ref{fig:abmPerfArm} shows these results.
\begin{figure}[h]
    \centering
    \includegraphics[width=14cm,keepaspectratio]{pics/abmPerfArm.png}
    \caption{ABM vs Serial Manager Performance Difference, ARM big.LITTLE Simulation}
    \label{fig:abmPerfArm}
\end{figure}
Since the previous testing demonstrated that the average processing speed did not affect the performance difference by much, it was eliminated from this test to simplify the presentation.\\\\
This testing is where the asynchronous management shows its strengths. Again the performance difference increases with the coefficient of variation, but now even no variation in workload sees a significant
improvement as the workers themselves add variation thanks to their differences in speed. The largest performance difference is seen when there is one weak worker, this worker will drive the serial 
management processing speed whereas it will play no effect on the asynchronous management, to the tune of an immediate 50 percent improvement. The performance improvement is immediate when 
there is one stronger worker in the distribution, a larger difference predictably occurring with less cores.\\\\
These results show us why actual measurements are needed, as the results for the strong worker, while still an improvement, are not as large as the theoretical difference. The theoretical performance increase
is 25 percent, whereas the actual difference was only around 15 percent. The calculation for this prediction is shown below:
\begin{align*}
    Increase &= \frac{x+x+x+2x}{x+x+x+x} = \frac{5}{4} = .25
\end{align*}
Where:\\
x = the processing speed of the workers\\
2x = the processing speed of the strong worker\\\\
I expected this value to be less than 25 percent due to the manager interaction overhead time, but this was significantly more than expected. The prediction here is that the longer the processing speed is 
for each worker, due to the observation in figure \ref{fig:abmPerf}, it is expected that this improvement will converge towards the theoretical value of 25 percent.\\\\
Despite the caveats and differences between the expected results and the actual results, it has been proven that in every test case, the ABM is at worst as good as serial management. The clear upside to using the
ABM is it has been shown to result in significant performance improvements with a simulated ARM big.LITTLE architecture and high worker workload variation, to the tune of well over 50 percent faster. The
increase in performance experienced will be based on the worker distribution and architecture it is implemented on, and can be predicted if some parameters of the cores and workers are known; we can do our best to map them
to these results, or create a simulation here inputing the parameters for the specific system we want to get an estimation for. 
