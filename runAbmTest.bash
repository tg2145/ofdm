baseSpeed=100
baseVariation=100
workerCounts=(1 2 3 4 5 6 7 8 9 10)
for var in ${workerCounts[@]}; do
    echo "Testing $var workers..."
    ./build/src/testAbm $var $baseSpeed $baseVariation
done
