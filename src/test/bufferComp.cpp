/*

Test to compare performance between async and serial buffer passing
Also allows you to simulate a "weak worker" to simulate what might happen with big.LITTLE arm architecture
Where some cores are purposely low energy consumption - low performance

*/

#include <iostream>
#include <chrono>
#include <cassert>
#include <cmath>

#include <multiPickupBuffer.hpp>
#include <multiDropoffBuffer.hpp>
#include <serialPickupBuffer.hpp>
#include <serialDropoffBuffer.hpp>

//#define ENABLE_WEAK_WORKER
//#define ENABLE_STRONG_WORKER
#define TEST_ITERATIONS 10000

std::size_t workerCount = 0;
std::size_t baseSpeed = 0;
std::size_t baseVariation = 0;
std::atomic<std::size_t> iterationCount = 0;

void printUsage()
{
    std::cout<<"Usage: bufferComp [workerCount] [baseSpeedUs] [maxVariationUs]\n";
}

void multiThreadFcn(std::size_t workerId, 
                    MultiPickupBuffer<std::array<int,10>>& pickup, 
                    MultiDropoffBuffer<std::array<int,10>>& dropoff)
{
    while((iterationCount++)<(TEST_ITERATIONS*workerCount))
    {
        auto pickMd = pickup.getRead(workerId);
        pickup.unlockRead(workerId);
        pickup.passRead(workerId);

        std::size_t amount = baseSpeed+rand()%baseVariation;
        #ifdef ENABLE_WEAK_WORKER
        if (workerId == 0) amount *= 2;
        #endif
        #ifdef ENABLE_STRONG_WORKER
        if (workerId == 0) amount /= 2;
        #endif
        std::this_thread::sleep_for(std::chrono::microseconds(amount));

        auto dropMd = dropoff.getWrite(pickMd.id);
        dropoff.unlockWrite(dropMd.id);
        dropoff.passWrite(dropMd.id, dropMd.bufferIndex);
    }
}

void serialThreadFcn(std::size_t workerId, 
                     SerialPickupBuffer<std::array<int,10>>& pickup, 
                     SerialDropoffBuffer<std::array<int,10>>& dropoff)
{
    for(std::size_t n=0;n<TEST_ITERATIONS;++n)
    {
        auto pickMd = pickup.getRead(workerId);
        static_cast<void>(pickMd);
        pickup.unlockRead(workerId);
        pickup.passRead(workerId);

        std::size_t amount = baseSpeed+rand()%baseVariation;
        #ifdef ENABLE_WEAK_WORKER
        if (workerId == 0) amount *= 2;
        #endif
        #ifdef ENABLE_STRONG_WORKER
        if (workerId == 0) amount /= 2;
        #endif
        std::this_thread::sleep_for(std::chrono::microseconds(amount));

        auto dropMd = dropoff.getWrite(workerId);
        dropoff.unlockWrite(workerId);
        dropoff.passWrite(dropMd.id, dropMd.bufferIndex);
    }
}

void multiWrite(std::size_t workerCount, MultiPickupBuffer<std::array<int,10>>& pickup)
{
    for(std::size_t n=0;n<TEST_ITERATIONS*workerCount;++n)
    {
        pickup.getWrite();
        pickup.passWrite();
    }
}
void multiConsume(std::size_t workerCount, MultiDropoffBuffer<std::array<int,10>>& dropoff)
{
    for(std::size_t n=0;n<TEST_ITERATIONS*workerCount;++n)
    {
        dropoff.getRead();
        dropoff.passRead();
    }
}

void serialWrite(std::size_t workerCount, SerialPickupBuffer<std::array<int,10>>& pickup)
{
    for(std::size_t n=0;n<TEST_ITERATIONS*workerCount;++n)
    {
        pickup.getWrite();
        pickup.passWrite();
    }
}
void serialConsume(std::size_t workerCount, SerialDropoffBuffer<std::array<int,10>>& dropoff)
{
    for(std::size_t n=0;n<TEST_ITERATIONS*workerCount;++n)
    {
        dropoff.getRead();
        dropoff.passRead();
    }
}

int main(int argc, char* argv[])
{
    if(argc != 4)
    {
        printUsage();
        return 0;
    }
    workerCount = strtol(argv[1], NULL, 10);
    baseSpeed = strtol(argv[2], NULL, 10);
    baseVariation = strtol(argv[3], NULL, 10);

    MultiPickupBuffer<std::array<int,10>> mpb(workerCount*3);
    MultiDropoffBuffer<std::array<int,10>> mdb(workerCount*3);
    SerialPickupBuffer<std::array<int,10>> spb(workerCount*3, workerCount);
    SerialDropoffBuffer<std::array<int,10>> sdb(workerCount*3, workerCount);

    srand(1); //ensure both tests get exactly the same sleep times assigned
    auto startM = std::chrono::steady_clock::now();
    std::thread workersM[workerCount];
    std::thread writerM(&multiWrite, workerCount, std::ref(mpb));
    std::thread consumerM(&multiConsume, workerCount, std::ref(mdb));
    for(std::size_t n=0;n<workerCount;++n)
    {
        workersM[n] = std::thread(&multiThreadFcn, n, std::ref(mpb), std::ref(mdb));
    }
    writerM.join();
    for(std::size_t n=0;n<workerCount;++n) workersM[n].join();
    consumerM.join();
    auto endM = std::chrono::steady_clock::now();
    auto timeM = std::chrono::duration_cast<std::chrono::milliseconds>(endM - startM).count();
    std::cout<<"Async Time: "<<timeM<<"\n";

    srand(1); //ensure both tests get exactly the same sleep times assigned
    auto startS = std::chrono::steady_clock::now();
    std::thread workersS[workerCount];
    std::thread writerS(&serialWrite, workerCount, std::ref(spb));
    std::thread consumerS(&serialConsume, workerCount, std::ref(sdb));
    for(std::size_t n=0;n<workerCount;++n)
    {
        workersS[n] = std::thread(&serialThreadFcn, n, std::ref(spb), std::ref(sdb));
    }
    writerS.join();
    for(std::size_t n=0;n<workerCount;++n) workersS[n].join();
    consumerS.join();
    auto endS = std::chrono::steady_clock::now();
    auto timeS = std::chrono::duration_cast<std::chrono::milliseconds>(endS - startS).count();
    std::cout<<"Serial Time: "<<timeS<<"\n";

    srand(1);
    double dataSum = 0.0;
    double squareDataSum = 0.0;
    for(std::size_t n=0;n<TEST_ITERATIONS*workerCount;++n)
    {
        double currentValue = static_cast<double>(baseSpeed+rand()%baseVariation);
        dataSum += currentValue;
        squareDataSum += currentValue * currentValue;
    }
    std::size_t length = TEST_ITERATIONS*workerCount;
    double mean = dataSum/static_cast<double>(length);
    double variance = ((length * squareDataSum) - (dataSum * dataSum)) / (length * (length - 1));
    double stdDeviation = sqrt(variance);
    std::cout<<"mean: "<<mean<<"\n";
    std::cout<<"variance: "<<variance<<"\n";
    std::cout<<"stdDeviation: "<<stdDeviation<<"\n";
    //calculate average sleep time, variation, std dev
    //see if a graph shows the relation
}