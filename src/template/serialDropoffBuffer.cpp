template <typename T>
SerialDropoffBuffer<T>::SerialDropoffBuffer(uint8_t bufferCount, std::size_t workerCount) :
workerCount(workerCount)
{
    block.resize(bufferCount);
    for(int16_t n=0;n<bufferCount;++n)
    {
        availableWriteBufferIndicies.push(n);
        writeLock.push_back(mutexPtr(new std::mutex()));
        readLock.push_back(mutexPtr(new std::mutex()));
        readLock[n]->lock();
    }
    for(std::size_t n=0;n<workerCount;++n)
    {
        workerSerialAccess.push_back(mutexPtr(new std::mutex()));
        workerSerialAccess[n]->lock();
    }
    workerSerialAccess[0]->unlock();//first worker can now write
}

template <typename T>
SerialDropoffBuffer<T>::~SerialDropoffBuffer(){}

template <typename T>
bool SerialDropoffBuffer<T>::checkForNextId()
{
    for(std::size_t n=0;n<readyReadBufferIndicies.size();++n)
    {
        if (readyReadBufferIndicies[n].id == nextReadId)
        {
            return true;
        }
    }
    return false;
}

template <typename T>
std::size_t SerialDropoffBuffer<T>::consumeNextId()
{
    readyReadBufferIndiciesLock.lock();
    std::size_t ret = 0;
    for(std::size_t n=0;n<readyReadBufferIndicies.size();++n)
    {
        if (readyReadBufferIndicies[n].id == nextReadId)
        {
            ret = readyReadBufferIndicies[n].index;
            readyReadBufferIndicies.erase(readyReadBufferIndicies.begin()+n);
        }
    }
    readyReadBufferIndiciesLock.unlock();
    return ret;
}

template <typename T>
void SerialDropoffBuffer<T>::passRead()
{
    ++nextReadId;
    writeLock[currentReadIndex]->unlock();
    ++readyWrite;
    ++currentMinimumId;
    availableWriteBufferIndicies.push(currentReadIndex);
}

template <typename T>
void SerialDropoffBuffer<T>::unlockWrite(std::size_t workerId)
{
    //next worker now has access, hence what makes this serial
    if(workerId == unlockId) workerSerialAccess[(workerId+1)%workerCount]->unlock();
}

template <typename T>
void SerialDropoffBuffer<T>::passWrite(std::size_t bufferId, std::size_t index)
{
    passWriteLock.lock();
    readLock[index]->unlock();
    readyReadBufferIndiciesLock.lock();
    readyReadBufferIndicies.push_back({index,bufferId});
    readyReadBufferIndiciesLock.unlock();
    ++readyRead;
    passWriteLock.unlock();
}

template <typename T>
typename std::vector<T>::iterator SerialDropoffBuffer<T>::begin() {return block.begin();}

template <typename T>
typename std::vector<T>::iterator SerialDropoffBuffer<T>::end() {return block.end();}

template <typename T>
int64_t SerialDropoffBuffer<T>::numReadyRead()
{
    return readyRead;
}

template <typename T>
int64_t SerialDropoffBuffer<T>::numReadyWrite()
{
    return readyWrite;
}