template <typename T>
SerialPickupBuffer<T>::SerialPickupBuffer(uint8_t bufferCount, std::size_t workerCount) :
workerCount(workerCount)
{
    workerMetaData.resize(workerCount);
    block.resize(bufferCount);
    for(int16_t n=0;n<bufferCount;++n)
    {
        writeLock.push_back(mutexPtr(new std::mutex()));
        readLock.push_back(mutexPtr(new std::mutex()));
        readLock[n]->lock();
    }
    for(std::size_t n=0;n<workerCount;++n)
    {
        workerSerialAccess.push_back(mutexPtr(new std::mutex()));
        workerSerialAccess[n]->lock();
    }
    workerSerialAccess[0]->unlock();//first worker can now read
}

template <typename T>
SerialPickupBuffer<T>::~SerialPickupBuffer(){}

template <typename T>
T& SerialPickupBuffer<T>::getWrite()
{
    writeLock[currentWriteBuffer]->lock();
    --readyWrite;
    return block[currentWriteBuffer];
}

template <typename T>
void SerialPickupBuffer<T>::unlockRead(std::size_t workerId)
{
    //next worker now has access, hence what makes this serial
    if(workerId == unlockId) workerSerialAccess[(workerId+1)%workerCount]->unlock();
}

template <typename T>
void SerialPickupBuffer<T>::passRead(std::size_t workerId)
{
    passReadLock.lock();
    std::size_t current = workerMetaData[workerId]%block.size();
    writeLock[current]->unlock();
    ++readyWrite;
    passReadLock.unlock();
}

template <typename T>
void SerialPickupBuffer<T>::passWrite()
{
    readLock[currentWriteBuffer]->unlock();
    ++readyRead;
    currentWriteBuffer = (currentWriteBuffer+1)%block.size();
}

template <typename T>
void SerialPickupBuffer<T>::passWrite(T& x)
{
    auto& y = getWrite();
    std::swap(x,y);
    passWrite();
}

template <typename T>
typename std::vector<T>::iterator SerialPickupBuffer<T>::begin() {return block.begin();}

template <typename T>
typename std::vector<T>::iterator SerialPickupBuffer<T>::end() {return block.end();}

template <typename T>
int64_t SerialPickupBuffer<T>::numReadyRead()
{
    return readyRead;
}

template <typename T>
int64_t SerialPickupBuffer<T>::numReadyWrite()
{
    return readyWrite;
}