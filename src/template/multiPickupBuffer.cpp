template <typename T>
MultiPickupBuffer<T>::MultiPickupBuffer(uint8_t bufferCount)
{
    workerMetaData.resize(bufferCount);
    block.resize(bufferCount);
    for(int16_t n=0;n<bufferCount;++n)
    {
        writeLock.push_back(mutexPtr(new std::mutex()));
        readLock.push_back(mutexPtr(new std::mutex()));
        readLock[n]->lock();
    }
}

template <typename T>
MultiPickupBuffer<T>::~MultiPickupBuffer(){}

template <typename T>
T& MultiPickupBuffer<T>::getWrite()
{
    writeLock[currentWriteBuffer]->lock();
    --readyWrite;
    return block[currentWriteBuffer];
}

template <typename T>
void MultiPickupBuffer<T>::unlockRead(std::size_t workerId)
{
    if(workerId == unlockId) getReadLock.unlock();
}

template <typename T>
void MultiPickupBuffer<T>::passRead(std::size_t workerId)
{
    passReadLock.lock();
    std::size_t current = workerMetaData[workerId]%block.size();
    writeLock[current]->unlock();
    ++readyWrite;
    passReadLock.unlock();
}

template <typename T>
void MultiPickupBuffer<T>::passWrite()
{
    readLock[currentWriteBuffer]->unlock();
    ++readyRead;
    currentWriteBuffer = (currentWriteBuffer+1)%block.size();
}

template <typename T>
void MultiPickupBuffer<T>::passWrite(T& x)
{
    auto& y = getWrite();
    std::swap(x,y);
    passWrite();
}

template <typename T>
typename std::vector<T>::iterator MultiPickupBuffer<T>::begin() {return block.begin();}

template <typename T>
typename std::vector<T>::iterator MultiPickupBuffer<T>::end() {return block.end();}

template <typename T>
int64_t MultiPickupBuffer<T>::numReadyRead()
{
    return readyRead;
}

template <typename T>
int64_t MultiPickupBuffer<T>::numReadyWrite()
{
    return readyWrite;
}