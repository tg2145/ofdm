template <typename T>
MultiDropoffBuffer<T>::MultiDropoffBuffer(uint8_t bufferCount)
{
    block.resize(bufferCount);
    for(int16_t n=0;n<bufferCount;++n)
    {
        availableWriteBufferIndicies.push(n);
        writeLock.push_back(mutex_ptr(new std::mutex()));
        readLock.push_back(mutex_ptr(new std::mutex()));
        readLock[n]->lock();
    }
}

template <typename T>
MultiDropoffBuffer<T>::~MultiDropoffBuffer(){}

template <typename T>
bool MultiDropoffBuffer<T>::checkForNextId()
{
    for(std::size_t n=0;n<readyReadBufferIndicies.size();++n)
    {
        if (readyReadBufferIndicies[n].id == nextReadId)
        {
            return true;
        }
    }
    return false;
}

template <typename T>
std::size_t MultiDropoffBuffer<T>::consumeNextId()
{
    readyReadBufferIndiciesLock.lock();
    std::size_t ret = 0;
    for(std::size_t n=0;n<readyReadBufferIndicies.size();++n)
    {
        if (readyReadBufferIndicies[n].id == nextReadId)
        {
            ret = readyReadBufferIndicies[n].index;
            readyReadBufferIndicies.erase(readyReadBufferIndicies.begin()+n);
        }
    }
    readyReadBufferIndiciesLock.unlock();
    return ret;
}

template <typename T>
void MultiDropoffBuffer<T>::unlockWrite(std::size_t id)
{
    if(id == unlockId) getWriteLock.unlock();
}

template <typename T>
void MultiDropoffBuffer<T>::passRead()
{
    ++nextReadId;
    writeLock[currentReadIndex]->unlock();
    ++readyWrite;
    ++currentMinimumId;
    availableWriteBufferIndicies.push(currentReadIndex);
}

template <typename T>
void MultiDropoffBuffer<T>::passWrite(std::size_t id, std::size_t index)
{
    passWriteLock.lock();
    readLock[index]->unlock();
    readyReadBufferIndiciesLock.lock();
    readyReadBufferIndicies.push_back({index,id});
    readyReadBufferIndiciesLock.unlock();
    ++readyRead;
    passWriteLock.unlock();
}

template <typename T>
typename std::vector<T>::iterator MultiDropoffBuffer<T>::begin() {return block.begin();}

template <typename T>
typename std::vector<T>::iterator MultiDropoffBuffer<T>::end() {return block.end();}

template <typename T>
int64_t MultiDropoffBuffer<T>::numReadyRead()
{
    return readyRead;
}

template <typename T>
int64_t MultiDropoffBuffer<T>::numReadyWrite()
{
    return readyWrite;
}