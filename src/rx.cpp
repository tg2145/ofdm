#include <rx.hpp>
#include <qpsk.hpp>
#include <multiPickupBuffer.hpp>

#include <iostream>
#include <vector>
#include <thread>
#include <cstring>
#include <chrono>
#include <mutex>

struct corrMd {
    int64_t corrI = 0;
    int64_t corrQ = 0;
    int64_t norm = 0;
    std::size_t length = 0;
};

struct corrRet {
    int64_t time = 0;
    uint64_t amount = 0;
};

struct RxImpl : public Rx
{
    public:
        RxImpl(std::size_t workers, MultiDropoffBuffer<OTABUFFER>* ota);
        ~RxImpl();
        void start();
        void wait();

    private:
        static constexpr std::size_t size = (SYMBOL_TIME_IN_SAMPLES - 2*OFFSET_BINS)*BITS_PER_SYMBOL;
        std::vector<fftwf_complex*> in, out;
        MultiDropoffBuffer<OTABUFFER>* ota;
        MultiPickupBuffer<std::array<std::complex<int16_t>,SYMBOL_TIME_IN_SAMPLES>> mpb;
        MultiDropoffBuffer<std::array<int,size>> mdb;
        std::vector<std::thread> threads;

        float threshold = 0.5;
        std::size_t workers;
        std::size_t testIterations = 30000;
        std::atomic<std::size_t> iterationsCompleted = 0;
        void signalGenerator();
        void worker(std::size_t myId);
        void consumer();
        void toCFloat(std::array<std::complex<int16_t>,SYMBOL_TIME_IN_SAMPLES>& input, fftwf_complex* output);
        corrMd correlate(std::complex<int16_t>* x, std::complex<int16_t>* y, std::size_t length);
        double toNormCorr(corrMd in);
        corrRet cpofdmCorrelator(std::vector<std::complex<int16_t>>& input);
};

RxImpl::RxImpl(std::size_t workers, MultiDropoffBuffer<OTABUFFER>* ota) :
    mpb(workers*3),
    mdb(workers*3),
    workers(workers)
{
    this->ota = ota;
    in.resize(workers);
    out.resize(workers);
    FFTW_LOCK.lock();
    for(std::size_t n=0;n<workers;++n)
    {
        in[n] = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex)*SYMBOL_TIME_IN_SAMPLES);
        out[n] = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex)*SYMBOL_TIME_IN_SAMPLES);
    }
    FFTW_LOCK.unlock();
}

RxImpl::~RxImpl() 
{
    for(std::size_t n=0;n<workers;++n)
    {
        fftwf_free(in[n]);
        fftwf_free(out[n]);
    }
}

corrMd
RxImpl::correlate(std::complex<int16_t>* x, std::complex<int16_t>* y, std::size_t length)
{
    int64_t corrI = 0, corrQ = 0;
    int64_t norm = 0;
    for(std::size_t n=0;n<length;++n)
    {
        corrI += static_cast<int32_t>(x[n].real())*static_cast<int32_t>(y[n].real())+
                 static_cast<int32_t>(x[n].imag())*static_cast<int32_t>(y[n].imag());
        corrQ += static_cast<int32_t>(x[n].imag())*static_cast<int32_t>(y[n].real())-
                 static_cast<int32_t>(x[n].real())*static_cast<int32_t>(y[n].imag());
        norm += static_cast<int32_t>(x[n].real())*static_cast<int32_t>(x[n].real())+
                static_cast<int32_t>(x[n].imag())*static_cast<int32_t>(x[n].imag());
    }
    return {corrI, corrQ, norm, length};
}

double
RxImpl::toNormCorr(corrMd in)
{
    return ((static_cast<double>(in.corrI)*
            static_cast<double>(in.corrI)+
            static_cast<double>(in.corrQ)*
            static_cast<double>(in.corrQ))/
            static_cast<double>(in.norm))/
            static_cast<double>(in.length);
}

corrRet
RxImpl::cpofdmCorrelator(std::vector<std::complex<int16_t>>& input)
{
    double prev = 0, curr = 0, next = 0; 
    corrMd currCorr, nextCorr;
    //get initial values
    auto start = std::chrono::steady_clock::now();
    nextCorr = correlate(&input[0], &input[SYMBOL_TIME_IN_SAMPLES], GUARD_INTERVAL_IN_SAMPLES);
    next = toNormCorr(nextCorr);
    //adjust only for next samples
    int total = 1;
    for(std::size_t n=1;n<input.size()-(SYMBOL_TIME_IN_SAMPLES+GUARD_INTERVAL_IN_SAMPLES);++n)
    {
        prev = curr;
        curr = next;
        currCorr = nextCorr;
        //remove sample corr from prev iteration
        nextCorr.corrI = currCorr.corrI - (static_cast<int32_t>(input[n-1].real())*static_cast<int32_t>(input[n-1+SYMBOL_TIME_IN_SAMPLES].real())+
                                           static_cast<int32_t>(input[n-1].imag())*static_cast<int32_t>(input[n-1+SYMBOL_TIME_IN_SAMPLES].imag()));
        nextCorr.corrQ = currCorr.corrQ - (static_cast<int32_t>(input[n-1].imag())*static_cast<int32_t>(input[n-1+SYMBOL_TIME_IN_SAMPLES].real())-
                                           static_cast<int32_t>(input[n-1].real())*static_cast<int32_t>(input[n-1+SYMBOL_TIME_IN_SAMPLES].imag()));
        nextCorr.norm = currCorr.norm - (static_cast<int32_t>(input[n-1].real())*static_cast<int32_t>(input[n-1].real())+
                                         static_cast<int32_t>(input[n-1].imag())*static_cast<int32_t>(input[n-1].imag()));
        //add sample corr for this iteration
        nextCorr.corrI += static_cast<int32_t>(input[n-1+GUARD_INTERVAL_IN_SAMPLES].real())*static_cast<int32_t>(input[n-1+SYMBOL_TIME_IN_SAMPLES+GUARD_INTERVAL_IN_SAMPLES].real())+
                          static_cast<int32_t>(input[n-1+GUARD_INTERVAL_IN_SAMPLES].imag())*static_cast<int32_t>(input[n-1+SYMBOL_TIME_IN_SAMPLES+GUARD_INTERVAL_IN_SAMPLES].imag());
        nextCorr.corrQ += static_cast<int32_t>(input[n-1+GUARD_INTERVAL_IN_SAMPLES].imag())*static_cast<int32_t>(input[n-1+SYMBOL_TIME_IN_SAMPLES+GUARD_INTERVAL_IN_SAMPLES].real())-
                          static_cast<int32_t>(input[n-1+GUARD_INTERVAL_IN_SAMPLES].real())*static_cast<int32_t>(input[n-1+SYMBOL_TIME_IN_SAMPLES+GUARD_INTERVAL_IN_SAMPLES].imag());
        nextCorr.norm += static_cast<int32_t>(input[n-1+GUARD_INTERVAL_IN_SAMPLES].real())*static_cast<int32_t>(input[n-1+GUARD_INTERVAL_IN_SAMPLES].real())+
                         static_cast<int32_t>(input[n-1+GUARD_INTERVAL_IN_SAMPLES].imag())*static_cast<int32_t>(input[n-1+GUARD_INTERVAL_IN_SAMPLES].imag());
        next = toNormCorr(nextCorr);
        if(curr > threshold)
        {
            //return n-1;
        }
        ++total;
    }
    auto end = std::chrono::steady_clock::now();
    auto corrTime = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
    //return -1
    return {corrTime, total}; //send processing time for first iteration for measurement purposes
}

void
RxImpl::signalGenerator()
{
    std::vector<std::complex<int16_t>> data(3*(SYMBOL_TIME_IN_SAMPLES+GUARD_INTERVAL_IN_SAMPLES));
    for(std::size_t n=0;n<data.size();++n)
    {
        data[n] = {static_cast<int16_t>(rand()%101-50), 
                   static_cast<int16_t>(rand()%101-50)};
    }
    auto result = cpofdmCorrelator(data);
    auto corrRate = static_cast<float>(result.amount)/(static_cast<float>(result.time)/1e9);
    std::cout<<"Corr time (full): "<<result.time<<"\n";
    std::cout<<"Corr rate: "<<corrRate<<" "<<result.amount<<"\n";
    for(std::size_t n=0;n<testIterations;++n)
    {
        std::size_t startIn = GUARD_INTERVAL_IN_SAMPLES;
        auto b = ota->getRead();
#if ADD_VAR
        std::vector<std::complex<int16_t>> temp(b.buffer.begin(), b.buffer.end());
        auto res = cpofdmCorrelator(temp);
        if (n<50) std::cout<<"Corr rate (small): "<<static_cast<float>(res.amount)/(static_cast<float>(res.time)/1e9)<<" "<<res.amount<<"\n";
        //correlate, add found index to startIn
#endif
        auto& buffer = mpb.getWrite();
        if (n < workers*3) std::copy(b.buffer.begin()+startIn, b.buffer.begin()+startIn+SYMBOL_TIME_IN_SAMPLES, buffer.begin());
        ota->passRead();
        mpb.passWrite();
    }
}

void
RxImpl::toCFloat(std::array<std::complex<int16_t>,SYMBOL_TIME_IN_SAMPLES>& input, fftwf_complex* output)
{
    for(std::size_t n=0;n<SYMBOL_TIME_IN_SAMPLES;++n)
    {
        output[n][0] = static_cast<float>(input[n].real());
        output[n][1] = static_cast<float>(input[n].imag());
    }
}

/*
data will be size: bits per symbol * (symbol time in samples - 2offset bins) 
*/
void
RxImpl::worker(std::size_t myId)
{
    fftwf_plan p;
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    FFTW_LOCK.lock();
    p = fftwf_plan_dft_1d(SYMBOL_TIME_IN_SAMPLES, &in[myId][0], &out[myId][0], FFTW_FORWARD, FFTW_ESTIMATE);
    FFTW_LOCK.unlock();
    while((iterationsCompleted++)<testIterations)
    {
        auto bufferMd = mpb.getRead(myId);
        mpb.unlockRead(myId);
        //we now have the identified symbol from the correlator which is feeding us the samples
        toCFloat(bufferMd.buffer, &in[myId][0]);
        mpb.passRead(myId);
        //undo the OFDM
        fftwf_execute(p);
        auto t = mdb.getWrite(bufferMd.id);
        mdb.unlockWrite(t.id);
        //map carrier symbols back to bits 
        fromQpsk(&out[myId][0], &t.buffer[0], t.buffer.size(), OFFSET_BINS);
        //pass bits to next stage
        mdb.passWrite(t.id, t.bufferIndex);
    }
    fftwf_destroy_plan(p);
}

//These will be the output bits
void 
RxImpl::consumer()
{
    for(std::size_t n=0;n<testIterations;++n) 
    {
        auto b = mdb.getRead();
#if COMPARE
        if(n == 0)
        {
            for(std::size_t m=0;m<24;++m) std::cout<<b.buffer[m];
            std::cout<<"\n";
        }
#else
        static_cast<void>(b);
#endif
        mdb.passRead();
    }
    //TODO compare to original bits or bytes
    //and initial prinout shows that it the first and last byte are the same -> points to all processing being correct
}

void
RxImpl::start()
{
    threads.push_back(std::thread(&RxImpl::signalGenerator, this));
    for(std::size_t n=0;n<workers;++n)
    {
        threads.push_back(std::thread(&RxImpl::worker, this, n));
    }
    threads.push_back(std::thread(&RxImpl::consumer, this));
}

void
RxImpl::wait()
{
    for(std::size_t n=0;n<threads.size();++n)
    {
        threads[n].join();
    }
}


Rx::Ptr
Rx::create(std::size_t workers, MultiDropoffBuffer<OTABUFFER>* ota)
{
    return Ptr{new RxImpl{workers,ota}};
}
