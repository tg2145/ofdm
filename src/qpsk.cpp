#include <qpsk.hpp>

void toQpsk(int *in, fftwf_complex *out, std::size_t size, std::size_t offsetBins)
{
    //TODO optimize to AVX2
    for(std::size_t n=0;n<size;n+=4)
    {
        out[n/4][0] = static_cast<float>(2*in[n]-1);
        out[n/4][1] = static_cast<float>(2*in[n+1]-1);

        out[SYMBOL_TIME_IN_SAMPLES-1-n/4][0] = static_cast<float>(2*in[n+2]-1);
        out[SYMBOL_TIME_IN_SAMPLES-1-n/4][1] = static_cast<float>(2*in[n+3]-1);
    }
}

//size here will be the amount of samples in
void fromQpsk(fftwf_complex *in, int *out, std::size_t size, std::size_t offsetBins)
{
    for(std::size_t n=0, start=0;n<(size-2*offsetBins)/2;++n, start+=4)
    {
        out[start] = in[n][0] > 0.0;
        out[start+1] = in[n][1] > 0.0;

        out[start+2] = in[SYMBOL_TIME_IN_SAMPLES-1-n][0] > 0.0;
        out[start+3] = in[SYMBOL_TIME_IN_SAMPLES-1-n][1] > 0.0;
    }
}