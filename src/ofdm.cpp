#include <iostream>
#include <chrono>
#include <cassert>
#include <cstdlib>

#include <tx.hpp>
#include <rx.hpp>

#include <constants.hpp>

void printUsage()
{
    std::cout<<"Usage: ofdm [workers per frontend]\n";
}

int main(int argc, char* argv[])
{
    if(argc != 2)
    {
        printUsage();
        return 0;
    }
    std::size_t workers = strtol(argv[1],NULL,10);
    std::cout<<"Using "<<workers<<" workers\n";
    std::cout<<SUBCARRIERS<<" subcarriers\n";
    std::cout<<"Symbol Rate: "<<SYMBOL_RATE<<"\n";
    std::cout<<"Bandwidth: "<<BANDWIDTH<<"\n";

    MultiDropoffBuffer<OTABUFFER> ota(workers*3);

    auto txPhy = Tx::create(workers, &ota);
#if not USING_UHD
    auto rxPhy = Rx::create(workers, &ota);
#endif

    auto start = std::chrono::steady_clock::now();
    txPhy->start();
#if not USING_UHD
    rxPhy->start();
#endif
    txPhy->wait();
#if not USING_UHD
    rxPhy->wait();
#endif
    auto end = std::chrono::steady_clock::now();

    auto modRate = static_cast<double>((SYMBOL_TIME_IN_SAMPLES+GUARD_INTERVAL_IN_SAMPLES)*30000)/
                    (std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count()/1e9);
    std::cout<<"Overall: "<<modRate<<"\n";
}