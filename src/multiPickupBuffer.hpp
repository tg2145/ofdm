#ifndef multiPickupBuffer_hpp
#define multiPickupBuffer_hpp

#include <mutex>
#include <vector>
#include <memory>
#include <atomic>
#include <cstdint>
#include <iostream>

/*
    Class that creates a block of buffers of type T that can be passed one way between threads
    Allows for thread safe data segment passing
*/

template <typename T>
class MultiPickupBuffer
{

public:
    struct Md {
        T& buffer;
        std::size_t id;
    };
    MultiPickupBuffer(uint8_t buffer_count);
    ~MultiPickupBuffer();
    Md getRead(std::size_t workerId)
    {
        getReadLock.lock();
        std::size_t current = currentGetReadBuffer%block.size();
        unlockId = workerId;
        workerMetaData[workerId] = currentGetReadBuffer;
        readLock[current]->lock();
        ++currentGetReadBuffer;
        --readyRead;
        return {block[current],currentGetReadBuffer-1};
    }

    void unlockRead(std::size_t workerId);
    T& getWrite();
    void passRead(std::size_t workerId);
    void passWrite();
    void passWrite(T& x);
    typename std::vector<T>::iterator begin();
    typename std::vector<T>::iterator end();
    int64_t numReadyRead();
    int64_t numReadyWrite();

private:
    using mutexPtr = std::unique_ptr<std::mutex>;
    std::vector<T> block;
    std::vector<mutexPtr> writeLock;
    std::vector<mutexPtr> readLock;
    std::mutex getReadLock, passReadLock;
    std::size_t currentGetReadBuffer = 0;
    std::size_t currentWriteBuffer = 0;
    std::size_t unlockId;
    uint8_t readyWrite;
    std::atomic<uint8_t> readyRead;
    std::vector<std::size_t> workerMetaData;

    std::chrono::high_resolution_clock::time_point unlockTime;
    std::chrono::high_resolution_clock::time_point lockTime;
};

#include <template/multiPickupBuffer.cpp>

#endif