#ifndef rx_hpp
#define rx_hpp

#include <memory>
#include <complex>
#include <constants.hpp>
#include <multiDropoffBuffer.hpp>

class Rx
{
public:
    using Ptr = std::unique_ptr<Rx>;
    static Ptr create(std::size_t workers, MultiDropoffBuffer<OTABUFFER>* ota);
    virtual ~Rx() = default;

    virtual void start() = 0;
    virtual void wait() = 0;
};

#endif