#ifndef constants_hpp
#define constants_hpp

#include <cstdlib>
#include <mutex>

#define USING_UHD 0
#define COMPARE 0
#define ADD_VAR 0

const std::size_t SAMPLE_RATE = 200e6; //max rate of X300
const std::size_t SYMBOL_TIME_IN_SAMPLES = 32768;
const std::size_t BITS_PER_SYMBOL = 2; // QPSK
const std::size_t FREQUENCY_OFFSET_GUARD = 25e6; //25e6 in both directions, for ~150MHz BW, 160MHz is max on X300, giving +-5MHz guard for freq correction 
constexpr std::size_t OFFSET_BINS = static_cast<double>(FREQUENCY_OFFSET_GUARD)/(static_cast<double>(SAMPLE_RATE)/static_cast<double>(SYMBOL_TIME_IN_SAMPLES));
constexpr std::size_t SUBCARRIERS = SYMBOL_TIME_IN_SAMPLES-2*OFFSET_BINS;
constexpr double BANDWIDTH = SUBCARRIERS*(static_cast<double>(SAMPLE_RATE)/static_cast<double>(SYMBOL_TIME_IN_SAMPLES));
constexpr double GUARD_INTERVAL = 1.0/32.0; //smallest guard interval, taken from DVBT
constexpr std::size_t GUARD_INTERVAL_IN_SAMPLES = static_cast<std::size_t>(static_cast<double>(SYMBOL_TIME_IN_SAMPLES)*GUARD_INTERVAL);
constexpr std::size_t TOTAL_SYMBOL_IN_SAMPLES = GUARD_INTERVAL_IN_SAMPLES+SYMBOL_TIME_IN_SAMPLES;
constexpr double SYMBOL_RATE = static_cast<double>(SAMPLE_RATE) / static_cast<double>(TOTAL_SYMBOL_IN_SAMPLES);

static std::mutex FFTW_LOCK;
const std::size_t VARIATION = 50; //room to allow correlator to detect signal

#if ADD_VAR
    using OTABUFFER = std::array<std::complex<int16_t>,TOTAL_SYMBOL_IN_SAMPLES+VARIATION>;
#else
    using OTABUFFER = std::array<std::complex<int16_t>,TOTAL_SYMBOL_IN_SAMPLES>;
#endif

#endif
