#ifndef tx_hpp
#define tx_hpp

#include <memory>
#include <complex>
#include <multiDropoffBuffer.hpp>
#include <constants.hpp>

class Tx
{
public:
    using Ptr = std::unique_ptr<Tx>;
    static Ptr create(std::size_t workers, MultiDropoffBuffer<OTABUFFER>* ota);
    virtual ~Tx() = default;

    virtual void start() = 0;
    virtual void wait() = 0;
};

#endif