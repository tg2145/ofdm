#ifndef qpsk_hpp
#define qpsk_hpp

#include <complex>
#include <cstdlib>
#include <fftw3.h>

#include <constants.hpp>

void toQpsk(int *in, fftwf_complex *out, std::size_t size, std::size_t offsetBins);
void fromQpsk(fftwf_complex *in, int *out, std::size_t size, std::size_t offsetBins);

#endif