#ifndef multiDropoffBuffer_hpp
#define multiDropoffBuffer_hpp

#include <mutex>
#include <vector>
#include <memory>
#include <atomic>
#include <cstdint>
#include <iostream>
#include <queue>
#include <thread>
#include <chrono>

/*
    Class that creates a block of buffers of type T that can be passed one way between threads
    Allows for thread safe data segment passing
*/

template <typename T>
class MultiDropoffBuffer
{

public:
    struct Md {
        T& buffer;
        std::size_t id;
        std::size_t bufferIndex; //to tie id to index
    };
    struct ReadMd {
        std::size_t index; //index tied to id
        std::size_t id;
    };
    MultiDropoffBuffer(uint8_t bufferCount);
    ~MultiDropoffBuffer();
    Md getRead()
    {
        while(!checkForNextId()){std::this_thread::sleep_for(std::chrono::microseconds(1));}
        currentReadIndex = consumeNextId();
        readLock[currentReadIndex]->lock();
        --readyRead;
        return {block[currentReadIndex],nextReadId,currentReadIndex};
    }
    Md getWrite(std::size_t id)
    {
        while (id >= currentMinimumId+block.size()){std::this_thread::sleep_for(std::chrono::microseconds(1));}
        getWriteLock.lock();
        while (availableWriteBufferIndicies.size() < 1)
        {
            std::this_thread::sleep_for(std::chrono::microseconds(1));
        }
        auto index = availableWriteBufferIndicies.front();
        unlockId = id;
        writeLock[index]->lock();
        --readyWrite;
        availableWriteBufferIndicies.pop();
        return {block[index],id,index};
    }
    void unlockWrite(std::size_t id);
    void passRead();
    void passWrite(std::size_t id, std::size_t index);
    typename std::vector<T>::iterator begin();
    typename std::vector<T>::iterator end();
    int64_t numReadyRead();
    int64_t numReadyWrite();

private:
    using mutex_ptr = std::unique_ptr<std::mutex>;
    std::size_t currentMinimumId = 0;
    std::size_t bufferCount;

    std::queue<std::size_t> availableWriteBufferIndicies;
    std::vector<mutex_ptr> writeLock;
    std::vector<mutex_ptr> readLock;

    std::mutex readyReadBufferIndiciesLock;
    std::vector<ReadMd> readyReadBufferIndicies;
    std::size_t nextReadId = 0;
    std::size_t unlockId;
    std::size_t currentReadIndex;

    std::mutex getWriteLock, passWriteLock;

    std::vector<T> block;

    uint8_t readyWrite;
    std::atomic<uint8_t> readyRead;

    std::chrono::high_resolution_clock::time_point unlockTime;
    std::chrono::high_resolution_clock::time_point lockTime;

    bool checkForNextId();
    std::size_t consumeNextId();
};

#include <template/multiDropoffBuffer.cpp>

#endif