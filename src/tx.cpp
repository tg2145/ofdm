#include <tx.hpp>
#include <qpsk.hpp>
#include <constants.hpp>
#include <multiPickupBuffer.hpp>
#include <multiDropoffBuffer.hpp>

#include <iostream>
#include <vector>
#include <thread>
#include <cstring>
#include <chrono>
#include <mutex>

#if USING_UHD
#include <uhd/usrp/multi_usrp.hpp>
#endif

struct TxImpl : public Tx
{
    public:
        TxImpl(std::size_t workers, MultiDropoffBuffer<OTABUFFER>* ota);
        ~TxImpl();
        void start();
        void wait();

    private:
        static constexpr std::size_t size = (SYMBOL_TIME_IN_SAMPLES - 2*OFFSET_BINS)*BITS_PER_SYMBOL;
        std::vector<fftwf_complex*> in, out;
        MultiPickupBuffer<std::array<int,size>> mpb;
        MultiDropoffBuffer<OTABUFFER>* mdb;
        std::vector<std::thread> threads;

#if USING_UHD
        uhd::usrp::multi_usrp::sptr dev;
        uhd::tx_streamer::sptr txStream;
#endif

        std::size_t workers;
        std::size_t testIterations = 30000;
        std::atomic<std::size_t> iterationsCompleted = 0;
        void dataGenerator();
        void worker(std::size_t myId);
        void consumer();
        void cpofdm(fftwf_complex* in, OTABUFFER& out);
};

TxImpl::TxImpl(std::size_t workers, MultiDropoffBuffer<OTABUFFER>* ota) :
    mpb(workers*3),
    workers(workers)
{
    mdb = ota;
    in.resize(workers);
    out.resize(workers);
    FFTW_LOCK.lock();
    for(std::size_t n=0;n<workers;++n)
    {
        in[n] = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex)*SYMBOL_TIME_IN_SAMPLES);
        out[n] = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex)*TOTAL_SYMBOL_IN_SAMPLES);
    }
    FFTW_LOCK.unlock();
    for(std::size_t id=0;id<workers;++id)
    {
        for(std::size_t n=0;n<SYMBOL_TIME_IN_SAMPLES;++n)
        {
            in[id][n][0] = 0;
            in[id][n][1] = 0;
        }
    }

#if USING_UHD
    dev = uhd::usrp::multi_usrp::make(uhd::device_addr_t());
    dev->set_tx_freq(2.2e9);
    dev->set_tx_rate(SAMPLE_RATE);
    dev->set_tx_bandwidth(160e6);
    dev->set_tx_gain(0);
    uhd::stream_args_t txArgs("sc16","sc16");
    txArgs.channels = {0};
    txStream = dev->get_tx_stream(txArgs);
#endif
}

TxImpl::~TxImpl() 
{
    for(std::size_t n=0;n<workers;++n)
    {
        fftwf_free(in[n]);
        fftwf_free(out[n]);
    }

}

void
TxImpl::dataGenerator()
{
    //they all now have data in them, so just reuse that data
    std::array<int, size> data;
    for(std::size_t n=0;n<size;++n)
    {
        data[n] = rand()%2;
#if COMPARE
        if(n<24) std::cout<<data[n];
#endif
    }
#if COMPARE
    std::cout<<"\n";
#endif
    for(std::size_t n=0;n<testIterations;++n)
    {
        auto& buffer = mpb.getWrite();
        if (n < workers*3) std::copy(data.begin(), data.end(), buffer.begin());
        mpb.passWrite();
    }
}

/*
inserts end of OFDM symbol to front as cyclic prefix
*/
void
TxImpl::cpofdm(fftwf_complex* in, OTABUFFER& out)
{
    int mul = 20; //scale to fill the DAC 
    std::size_t start = 0;
#if ADD_VAR
    start = 35;
#endif
    for(std::size_t n=0;n<GUARD_INTERVAL_IN_SAMPLES;++n)
    {
        out[n+start] = {static_cast<int16_t>(mul*in[SYMBOL_TIME_IN_SAMPLES+n][0]), 
                        static_cast<int16_t>(mul*in[SYMBOL_TIME_IN_SAMPLES+n][1])};
    }
    for(std::size_t n=0;n<SYMBOL_TIME_IN_SAMPLES;++n)
    {
        out[GUARD_INTERVAL_IN_SAMPLES+n+start] = {static_cast<int16_t>(mul*in[GUARD_INTERVAL_IN_SAMPLES+n][0]), 
                                                  static_cast<int16_t>(mul*in[GUARD_INTERVAL_IN_SAMPLES+n][1])};
    }
}

/*
data will be size: bits per symbol * (symbol time in samples - 2offset bins) 
*/
void
TxImpl::worker(std::size_t myId)
{
    fftwf_plan p;
    FFTW_LOCK.lock();
    p = fftwf_plan_dft_1d(SYMBOL_TIME_IN_SAMPLES, &in[myId][0], &out[myId][GUARD_INTERVAL_IN_SAMPLES], FFTW_BACKWARD, FFTW_ESTIMATE);
    FFTW_LOCK.unlock();
    while((iterationsCompleted++)<testIterations)
    {
        //~100ns
        auto bufferMd = mpb.getRead(myId);
        mpb.unlockRead(myId);
        //~40us
        toQpsk(&bufferMd.buffer[0], &in[myId][0], bufferMd.buffer.size(), OFFSET_BINS);
        //~2.5us
        mpb.passRead(myId);
        //300us
        fftwf_execute(p);
        //300ns
        auto t = mdb->getWrite(bufferMd.id);
        mdb->unlockWrite(t.id);
        //7us
        cpofdm(&out[myId][0], t.buffer);
        //150ns
        mdb->passWrite(t.id, t.bufferIndex);
    }
    fftwf_destroy_plan(p);
}

//simulates transmission
void 
TxImpl::consumer()
{
    for(std::size_t n=0;n<testIterations;++n) 
    {
        auto b = mdb->getRead();
#if USING_UHD
        uhd::tx_metadata_t md;
        txStream->send(b.buffer.data(),b.buffer.size(),md,10);
#else
        static_cast<void>(b);
#endif
        mdb->passRead();
    }
}

void
TxImpl::start()
{
    threads.push_back(std::thread(&TxImpl::dataGenerator, this));
    for(std::size_t n=0;n<workers;++n)
    {
        threads.push_back(std::thread(&TxImpl::worker, this, n));
    }
#if USING_UHD
    threads.push_back(std::thread(&TxImpl::consumer, this));
#endif
}

void
TxImpl::wait()
{
    for(std::size_t n=0;n<threads.size();++n)
    {
        threads[n].join();
    }
}

Tx::Ptr
Tx::create(std::size_t workers, MultiDropoffBuffer<OTABUFFER>* ota)
{
    return Ptr{new TxImpl{workers,ota}};
}
